﻿using UnityEngine;

namespace Core.Scripts
{
    public class EnvironmentController : MonoBehaviour
    {
        [SerializeField] private GameObject playerOneCube01;
        [SerializeField] private GameObject playerOneCube02;
        [SerializeField] private GameObject playerOneCube03;
        [SerializeField] private GameObject playerOneCube04;
        [SerializeField] private GameObject playerOneCube05;
        [SerializeField] private GameObject playerOneCube06;
        [SerializeField] private GameObject playerOneCube07;
        [SerializeField] private GameObject playerOneCube08;
        [SerializeField] private GameObject playerOneCube09;
        [SerializeField] private GameObject playerTwoCube01;
        [SerializeField] private GameObject playerTwoCube02;
        [SerializeField] private GameObject playerTwoCube03;
        [SerializeField] private GameObject playerTwoCube04;
        [SerializeField] private GameObject playerTwoCube05;
        [SerializeField] private GameObject playerTwoCube06;
        [SerializeField] private GameObject playerTwoCube07;
        [SerializeField] private GameObject playerTwoCube08;
        [SerializeField] private GameObject playerTwoCube09;

        public static int attackNumber;

        private Animator playerOneCube01Animator;
        private Animator playerOneCube02Animator;
        private Animator playerOneCube03Animator;
        private Animator playerOneCube04Animator;
        private Animator playerOneCube05Animator;
        private Animator playerOneCube06Animator;
        private Animator playerOneCube07Animator;
        private Animator playerOneCube08Animator;
        private Animator playerOneCube09Animator;
        private Animator playerTwoCube01Animator;
        private Animator playerTwoCube02Animator;
        private Animator playerTwoCube03Animator;
        private Animator playerTwoCube04Animator;
        private Animator playerTwoCube05Animator;
        private Animator playerTwoCube06Animator;
        private Animator playerTwoCube07Animator;
        private Animator playerTwoCube08Animator;
        private Animator playerTwoCube09Animator;

        private void Start()
        {
            playerOneCube01Animator = playerOneCube01.GetComponent<Animator>();
            playerOneCube02Animator = playerOneCube02.GetComponent<Animator>();
            playerOneCube03Animator = playerOneCube03.GetComponent<Animator>();
            playerOneCube04Animator = playerOneCube04.GetComponent<Animator>();
            playerOneCube05Animator = playerOneCube05.GetComponent<Animator>();
            playerOneCube06Animator = playerOneCube06.GetComponent<Animator>();
            playerOneCube07Animator = playerOneCube07.GetComponent<Animator>();
            playerOneCube08Animator = playerOneCube08.GetComponent<Animator>();
            playerOneCube09Animator = playerOneCube09.GetComponent<Animator>();
            playerTwoCube01Animator = playerTwoCube01.GetComponent<Animator>();
            playerTwoCube02Animator = playerTwoCube02.GetComponent<Animator>();
            playerTwoCube03Animator = playerTwoCube03.GetComponent<Animator>();
            playerTwoCube04Animator = playerTwoCube04.GetComponent<Animator>();
            playerTwoCube05Animator = playerTwoCube05.GetComponent<Animator>();
            playerTwoCube06Animator = playerTwoCube06.GetComponent<Animator>();
            playerTwoCube07Animator = playerTwoCube07.GetComponent<Animator>();
            playerTwoCube08Animator = playerTwoCube08.GetComponent<Animator>();
            playerTwoCube09Animator = playerTwoCube09.GetComponent<Animator>();
        }

        private void FixedUpdate()
        {
            if (!PlayerHealth.isDead && GameSettings.isSpellCastingOn)
            {
                if (PlayerShooting.bulletSpecialCooldownCount > 0.0f)
                {
                    if (!GameManager.isPlayerTwo)
                    {
                        if (playerTwoCube01Animator.GetBool("isOn")) playerTwoCube01Animator.SetBool("isOn", false);
                        if (playerTwoCube02Animator.GetBool("isOn")) playerTwoCube02Animator.SetBool("isOn", false);
                        if (playerTwoCube03Animator.GetBool("isOn")) playerTwoCube03Animator.SetBool("isOn", false);
                        if (playerTwoCube04Animator.GetBool("isOn")) playerTwoCube04Animator.SetBool("isOn", false);
                        if (playerTwoCube05Animator.GetBool("isOn")) playerTwoCube05Animator.SetBool("isOn", false);
                        if (playerTwoCube06Animator.GetBool("isOn")) playerTwoCube06Animator.SetBool("isOn", false);
                        if (playerTwoCube07Animator.GetBool("isOn")) playerTwoCube07Animator.SetBool("isOn", false);
                        if (playerTwoCube08Animator.GetBool("isOn")) playerTwoCube08Animator.SetBool("isOn", false);
                        if (playerTwoCube09Animator.GetBool("isOn")) playerTwoCube09Animator.SetBool("isOn", false);
                    }
                    else
                    {
                        if (playerOneCube01Animator.GetBool("isOn")) playerOneCube01Animator.SetBool("isOn", false);
                        if (playerOneCube02Animator.GetBool("isOn")) playerOneCube02Animator.SetBool("isOn", false);
                        if (playerOneCube03Animator.GetBool("isOn")) playerOneCube03Animator.SetBool("isOn", false);
                        if (playerOneCube04Animator.GetBool("isOn")) playerOneCube04Animator.SetBool("isOn", false);
                        if (playerOneCube05Animator.GetBool("isOn")) playerOneCube05Animator.SetBool("isOn", false);
                        if (playerOneCube06Animator.GetBool("isOn")) playerOneCube06Animator.SetBool("isOn", false);
                        if (playerOneCube07Animator.GetBool("isOn")) playerOneCube07Animator.SetBool("isOn", false);
                        if (playerOneCube08Animator.GetBool("isOn")) playerOneCube08Animator.SetBool("isOn", false);
                        if (playerOneCube09Animator.GetBool("isOn")) playerOneCube09Animator.SetBool("isOn", false);
                    }
                }
                else
                {
                    if (PlayerShooting.indexSpell01 == 0)
                    {
                        if (!GameManager.isPlayerTwo)
                        {
                            if (PlayerMovement.targetPosition.x > -7.5f) // Горизонтальная верхняя линия (130)
                            {
                                if (!playerTwoCube07Animator.GetBool("isOn")) playerTwoCube07Animator.SetBool("isOn", true);
                                if (!playerTwoCube08Animator.GetBool("isOn")) playerTwoCube08Animator.SetBool("isOn", true);
                                if (!playerTwoCube09Animator.GetBool("isOn")) playerTwoCube09Animator.SetBool("isOn", true);
                            
                                if (playerTwoCube01Animator.GetBool("isOn")) playerTwoCube01Animator.SetBool("isOn", false);
                                if (playerTwoCube02Animator.GetBool("isOn")) playerTwoCube02Animator.SetBool("isOn", false);
                                if (playerTwoCube03Animator.GetBool("isOn")) playerTwoCube03Animator.SetBool("isOn", false);
                                if (playerTwoCube04Animator.GetBool("isOn")) playerTwoCube04Animator.SetBool("isOn", false);
                                if (playerTwoCube05Animator.GetBool("isOn")) playerTwoCube05Animator.SetBool("isOn", false);
                                if (playerTwoCube06Animator.GetBool("isOn")) playerTwoCube06Animator.SetBool("isOn", false);

                                attackNumber = 130;
                            }
                            else if (PlayerMovement.targetPosition.x > -12.5f) // Горизонтальная средняя линия (120)
                            {
                                if (!playerTwoCube04Animator.GetBool("isOn")) playerTwoCube04Animator.SetBool("isOn", true);
                                if (!playerTwoCube05Animator.GetBool("isOn")) playerTwoCube05Animator.SetBool("isOn", true);
                                if (!playerTwoCube06Animator.GetBool("isOn")) playerTwoCube06Animator.SetBool("isOn", true);
                            
                                if (playerTwoCube01Animator.GetBool("isOn")) playerTwoCube01Animator.SetBool("isOn", false);
                                if (playerTwoCube02Animator.GetBool("isOn")) playerTwoCube02Animator.SetBool("isOn", false);
                                if (playerTwoCube03Animator.GetBool("isOn")) playerTwoCube03Animator.SetBool("isOn", false);
                                if (playerTwoCube07Animator.GetBool("isOn")) playerTwoCube07Animator.SetBool("isOn", false);
                                if (playerTwoCube08Animator.GetBool("isOn")) playerTwoCube08Animator.SetBool("isOn", false);
                                if (playerTwoCube09Animator.GetBool("isOn")) playerTwoCube09Animator.SetBool("isOn", false);
                            
                                attackNumber = 120;
                            }
                            else // Горизонтальная нижняя линия (110)
                            {
                                if (!playerTwoCube01Animator.GetBool("isOn")) playerTwoCube01Animator.SetBool("isOn", true);
                                if (!playerTwoCube02Animator.GetBool("isOn")) playerTwoCube02Animator.SetBool("isOn", true);
                                if (!playerTwoCube03Animator.GetBool("isOn")) playerTwoCube03Animator.SetBool("isOn", true);
                            
                                if (playerTwoCube04Animator.GetBool("isOn")) playerTwoCube04Animator.SetBool("isOn", false);
                                if (playerTwoCube05Animator.GetBool("isOn")) playerTwoCube05Animator.SetBool("isOn", false);
                                if (playerTwoCube06Animator.GetBool("isOn")) playerTwoCube06Animator.SetBool("isOn", false);
                                if (playerTwoCube07Animator.GetBool("isOn")) playerTwoCube07Animator.SetBool("isOn", false);
                                if (playerTwoCube08Animator.GetBool("isOn")) playerTwoCube08Animator.SetBool("isOn", false);
                                if (playerTwoCube09Animator.GetBool("isOn")) playerTwoCube09Animator.SetBool("isOn", false);
                            
                                attackNumber = 110;
                            }
                        }
                        else
                        {
                            if (PlayerMovement.targetPosition.x > 10.0f) // Горизонтальная нижняя линия (110)
                            {
                                if (!playerOneCube07Animator.GetBool("isOn")) playerOneCube07Animator.SetBool("isOn", true);
                                if (!playerOneCube08Animator.GetBool("isOn")) playerOneCube08Animator.SetBool("isOn", true);
                                if (!playerOneCube09Animator.GetBool("isOn")) playerOneCube09Animator.SetBool("isOn", true);
                            
                                if (playerOneCube01Animator.GetBool("isOn")) playerOneCube01Animator.SetBool("isOn", false);
                                if (playerOneCube02Animator.GetBool("isOn")) playerOneCube02Animator.SetBool("isOn", false);
                                if (playerOneCube03Animator.GetBool("isOn")) playerOneCube03Animator.SetBool("isOn", false);
                                if (playerOneCube04Animator.GetBool("isOn")) playerOneCube04Animator.SetBool("isOn", false);
                                if (playerOneCube05Animator.GetBool("isOn")) playerOneCube05Animator.SetBool("isOn", false);
                                if (playerOneCube06Animator.GetBool("isOn")) playerOneCube06Animator.SetBool("isOn", false);
                            
                                attackNumber = 110;
                            }
                            else if (PlayerMovement.targetPosition.x > 5.0f) // Горизонтальная средняя линия (120)
                            {
                                if (!playerOneCube04Animator.GetBool("isOn")) playerOneCube04Animator.SetBool("isOn", true);
                                if (!playerOneCube05Animator.GetBool("isOn")) playerOneCube05Animator.SetBool("isOn", true);
                                if (!playerOneCube06Animator.GetBool("isOn")) playerOneCube06Animator.SetBool("isOn", true);
                            
                                if (playerOneCube01Animator.GetBool("isOn")) playerOneCube01Animator.SetBool("isOn", false);
                                if (playerOneCube02Animator.GetBool("isOn")) playerOneCube02Animator.SetBool("isOn", false);
                                if (playerOneCube03Animator.GetBool("isOn")) playerOneCube03Animator.SetBool("isOn", false);
                                if (playerOneCube07Animator.GetBool("isOn")) playerOneCube07Animator.SetBool("isOn", false);
                                if (playerOneCube08Animator.GetBool("isOn")) playerOneCube08Animator.SetBool("isOn", false);
                                if (playerOneCube09Animator.GetBool("isOn")) playerOneCube09Animator.SetBool("isOn", false);
                            
                                attackNumber = 120;
                            }
                            else // Горизонтальная верхняя линия (130)
                            {
                                if (!playerOneCube01Animator.GetBool("isOn")) playerOneCube01Animator.SetBool("isOn", true);
                                if (!playerOneCube02Animator.GetBool("isOn")) playerOneCube02Animator.SetBool("isOn", true);
                                if (!playerOneCube03Animator.GetBool("isOn")) playerOneCube03Animator.SetBool("isOn", true);
                            
                                if (playerOneCube04Animator.GetBool("isOn")) playerOneCube04Animator.SetBool("isOn", false);
                                if (playerOneCube05Animator.GetBool("isOn")) playerOneCube05Animator.SetBool("isOn", false);
                                if (playerOneCube06Animator.GetBool("isOn")) playerOneCube06Animator.SetBool("isOn", false);
                                if (playerOneCube07Animator.GetBool("isOn")) playerOneCube07Animator.SetBool("isOn", false);
                                if (playerOneCube08Animator.GetBool("isOn")) playerOneCube08Animator.SetBool("isOn", false);
                                if (playerOneCube09Animator.GetBool("isOn")) playerOneCube09Animator.SetBool("isOn", false);
                            
                                attackNumber = 130;
                            }
                        }
                    }
                    else if (PlayerShooting.indexSpell01 == 1)
                    {
                        if (!GameManager.isPlayerTwo)
                        {
                            if (PlayerMovement.targetPosition.z > 1.5f) // Вертикальная левая линия (210)
                            {
                                if (!playerTwoCube01Animator.GetBool("isOn")) playerTwoCube01Animator.SetBool("isOn", true);
                                if (!playerTwoCube04Animator.GetBool("isOn")) playerTwoCube04Animator.SetBool("isOn", true);
                                if (!playerTwoCube07Animator.GetBool("isOn")) playerTwoCube07Animator.SetBool("isOn", true);
                        
                                if (playerTwoCube02Animator.GetBool("isOn")) playerTwoCube02Animator.SetBool("isOn", false);
                                if (playerTwoCube03Animator.GetBool("isOn")) playerTwoCube03Animator.SetBool("isOn", false);
                                if (playerTwoCube05Animator.GetBool("isOn")) playerTwoCube05Animator.SetBool("isOn", false);
                                if (playerTwoCube06Animator.GetBool("isOn")) playerTwoCube06Animator.SetBool("isOn", false);
                                if (playerTwoCube08Animator.GetBool("isOn")) playerTwoCube08Animator.SetBool("isOn", false);
                                if (playerTwoCube09Animator.GetBool("isOn")) playerTwoCube09Animator.SetBool("isOn", false);
                            
                                attackNumber = 210;
                            }
                            else if (PlayerMovement.targetPosition.z < -1.5f) // Вертикальная правая линия (230)
                            {
                                if (!playerTwoCube03Animator.GetBool("isOn")) playerTwoCube03Animator.SetBool("isOn", true);
                                if (!playerTwoCube06Animator.GetBool("isOn")) playerTwoCube06Animator.SetBool("isOn", true);
                                if (!playerTwoCube09Animator.GetBool("isOn")) playerTwoCube09Animator.SetBool("isOn", true);
                        
                                if (playerTwoCube01Animator.GetBool("isOn")) playerTwoCube01Animator.SetBool("isOn", false);
                                if (playerTwoCube02Animator.GetBool("isOn")) playerTwoCube02Animator.SetBool("isOn", false);
                                if (playerTwoCube04Animator.GetBool("isOn")) playerTwoCube04Animator.SetBool("isOn", false);
                                if (playerTwoCube05Animator.GetBool("isOn")) playerTwoCube05Animator.SetBool("isOn", false);
                                if (playerTwoCube07Animator.GetBool("isOn")) playerTwoCube07Animator.SetBool("isOn", false);
                                if (playerTwoCube08Animator.GetBool("isOn")) playerTwoCube08Animator.SetBool("isOn", false);
                            
                                attackNumber = 230;
                            }
                            else // Вертикальная средняя линия (220)
                            {
                                if (!playerTwoCube02Animator.GetBool("isOn")) playerTwoCube02Animator.SetBool("isOn", true);
                                if (!playerTwoCube05Animator.GetBool("isOn")) playerTwoCube05Animator.SetBool("isOn", true);
                                if (!playerTwoCube08Animator.GetBool("isOn")) playerTwoCube08Animator.SetBool("isOn", true);
                        
                                if (playerTwoCube01Animator.GetBool("isOn")) playerTwoCube01Animator.SetBool("isOn", false);
                                if (playerTwoCube03Animator.GetBool("isOn")) playerTwoCube03Animator.SetBool("isOn", false);
                                if (playerTwoCube04Animator.GetBool("isOn")) playerTwoCube04Animator.SetBool("isOn", false);
                                if (playerTwoCube06Animator.GetBool("isOn")) playerTwoCube06Animator.SetBool("isOn", false);
                                if (playerTwoCube07Animator.GetBool("isOn")) playerTwoCube07Animator.SetBool("isOn", false);
                                if (playerTwoCube09Animator.GetBool("isOn")) playerTwoCube09Animator.SetBool("isOn", false);
                            
                                attackNumber = 220;
                            }
                        }
                        else
                        {
                            if (PlayerMovement.targetPosition.z > 1.5f) // Вертикальная правая линия (230)
                            {
                                if (!playerOneCube01Animator.GetBool("isOn")) playerOneCube01Animator.SetBool("isOn", true);
                                if (!playerOneCube04Animator.GetBool("isOn")) playerOneCube04Animator.SetBool("isOn", true);
                                if (!playerOneCube07Animator.GetBool("isOn")) playerOneCube07Animator.SetBool("isOn", true);
                        
                                if (playerOneCube02Animator.GetBool("isOn")) playerOneCube02Animator.SetBool("isOn", false);
                                if (playerOneCube03Animator.GetBool("isOn")) playerOneCube03Animator.SetBool("isOn", false);
                                if (playerOneCube05Animator.GetBool("isOn")) playerOneCube05Animator.SetBool("isOn", false);
                                if (playerOneCube06Animator.GetBool("isOn")) playerOneCube06Animator.SetBool("isOn", false);
                                if (playerOneCube08Animator.GetBool("isOn")) playerOneCube08Animator.SetBool("isOn", false);
                                if (playerOneCube09Animator.GetBool("isOn")) playerOneCube09Animator.SetBool("isOn", false);
                            
                                attackNumber = 230;
                            }
                            else if (PlayerMovement.targetPosition.z < -1.5f) // Вертикальная левая линия (210)
                            {
                                if (!playerOneCube03Animator.GetBool("isOn")) playerOneCube03Animator.SetBool("isOn", true);
                                if (!playerOneCube06Animator.GetBool("isOn")) playerOneCube06Animator.SetBool("isOn", true);
                                if (!playerOneCube09Animator.GetBool("isOn")) playerOneCube09Animator.SetBool("isOn", true);
                        
                                if (playerOneCube01Animator.GetBool("isOn")) playerOneCube01Animator.SetBool("isOn", false);
                                if (playerOneCube02Animator.GetBool("isOn")) playerOneCube02Animator.SetBool("isOn", false);
                                if (playerOneCube04Animator.GetBool("isOn")) playerOneCube04Animator.SetBool("isOn", false);
                                if (playerOneCube05Animator.GetBool("isOn")) playerOneCube05Animator.SetBool("isOn", false);
                                if (playerOneCube07Animator.GetBool("isOn")) playerOneCube07Animator.SetBool("isOn", false);
                                if (playerOneCube08Animator.GetBool("isOn")) playerOneCube08Animator.SetBool("isOn", false);
                            
                                attackNumber = 210;
                            }
                            else // Вертикальная средняя линия (220)
                            {
                                if (!playerOneCube02Animator.GetBool("isOn")) playerOneCube02Animator.SetBool("isOn", true);
                                if (!playerOneCube05Animator.GetBool("isOn")) playerOneCube05Animator.SetBool("isOn", true);
                                if (!playerOneCube08Animator.GetBool("isOn")) playerOneCube08Animator.SetBool("isOn", true);
                        
                                if (playerOneCube01Animator.GetBool("isOn")) playerOneCube01Animator.SetBool("isOn", false);
                                if (playerOneCube03Animator.GetBool("isOn")) playerOneCube03Animator.SetBool("isOn", false);
                                if (playerOneCube04Animator.GetBool("isOn")) playerOneCube04Animator.SetBool("isOn", false);
                                if (playerOneCube06Animator.GetBool("isOn")) playerOneCube06Animator.SetBool("isOn", false);
                                if (playerOneCube07Animator.GetBool("isOn")) playerOneCube07Animator.SetBool("isOn", false);
                                if (playerOneCube09Animator.GetBool("isOn")) playerOneCube09Animator.SetBool("isOn", false);
                            
                                attackNumber = 220;
                            }
                        }
                    }
                    else if (PlayerShooting.indexSpell01 == 2)
                    {
                        if (!GameManager.isPlayerTwo)
                        {
                            if (PlayerMovement.targetPosition.x > -7.5f) 
                            {
                                if (!playerTwoCube07Animator.GetBool("isOn")) playerTwoCube07Animator.SetBool("isOn", true);
                                if (!playerTwoCube08Animator.GetBool("isOn")) playerTwoCube08Animator.SetBool("isOn", true);
                                if (!playerTwoCube09Animator.GetBool("isOn")) playerTwoCube09Animator.SetBool("isOn", true);
                        
                                if (PlayerMovement.targetPosition.z > 1.5f) // Горизонтальная верхняя и вертикальная левая линии (331)
                                {
                                    if (!playerTwoCube01Animator.GetBool("isOn")) playerTwoCube01Animator.SetBool("isOn", true);
                                    if (!playerTwoCube04Animator.GetBool("isOn")) playerTwoCube04Animator.SetBool("isOn", true);
                        
                                    if (playerTwoCube02Animator.GetBool("isOn")) playerTwoCube02Animator.SetBool("isOn", false);
                                    if (playerTwoCube03Animator.GetBool("isOn")) playerTwoCube03Animator.SetBool("isOn", false);
                                    if (playerTwoCube05Animator.GetBool("isOn")) playerTwoCube05Animator.SetBool("isOn", false);
                                    if (playerTwoCube06Animator.GetBool("isOn")) playerTwoCube06Animator.SetBool("isOn", false);
                                
                                    attackNumber = 331;
                                }
                                else if (PlayerMovement.targetPosition.z < -1.5f) // Горизонтальная верхняя и вертикальная правая линии (333)
                                {
                                    if (!playerTwoCube03Animator.GetBool("isOn")) playerTwoCube03Animator.SetBool("isOn", true);
                                    if (!playerTwoCube06Animator.GetBool("isOn")) playerTwoCube06Animator.SetBool("isOn", true);
                        
                                    if (playerTwoCube01Animator.GetBool("isOn")) playerTwoCube01Animator.SetBool("isOn", false);
                                    if (playerTwoCube02Animator.GetBool("isOn")) playerTwoCube02Animator.SetBool("isOn", false);
                                    if (playerTwoCube04Animator.GetBool("isOn")) playerTwoCube04Animator.SetBool("isOn", false);
                                    if (playerTwoCube05Animator.GetBool("isOn")) playerTwoCube05Animator.SetBool("isOn", false);
                                
                                    attackNumber = 333;
                                }
                                else // Горизонтальная верхняя и вертикальная средняя линии (332)
                                {
                                    if (!playerTwoCube02Animator.GetBool("isOn")) playerTwoCube02Animator.SetBool("isOn", true);
                                    if (!playerTwoCube05Animator.GetBool("isOn")) playerTwoCube05Animator.SetBool("isOn", true);
                        
                                    if (playerTwoCube01Animator.GetBool("isOn")) playerTwoCube01Animator.SetBool("isOn", false);
                                    if (playerTwoCube03Animator.GetBool("isOn")) playerTwoCube03Animator.SetBool("isOn", false);
                                    if (playerTwoCube04Animator.GetBool("isOn")) playerTwoCube04Animator.SetBool("isOn", false);
                                    if (playerTwoCube06Animator.GetBool("isOn")) playerTwoCube06Animator.SetBool("isOn", false);
                                
                                    attackNumber = 332;
                                }
                            }
                            else if (PlayerMovement.targetPosition.x > -12.5f) 
                            {
                                if (!playerTwoCube04Animator.GetBool("isOn")) playerTwoCube04Animator.SetBool("isOn", true);
                                if (!playerTwoCube05Animator.GetBool("isOn")) playerTwoCube05Animator.SetBool("isOn", true);
                                if (!playerTwoCube06Animator.GetBool("isOn")) playerTwoCube06Animator.SetBool("isOn", true);
                        
                                if (PlayerMovement.targetPosition.z > 1.5f) // Горизонтальная средняя и вертикальная левая линии (321)
                                {
                                    if (!playerTwoCube01Animator.GetBool("isOn")) playerTwoCube01Animator.SetBool("isOn", true);
                                    if (!playerTwoCube07Animator.GetBool("isOn")) playerTwoCube07Animator.SetBool("isOn", true);
                        
                                    if (playerTwoCube02Animator.GetBool("isOn")) playerTwoCube02Animator.SetBool("isOn", false);
                                    if (playerTwoCube03Animator.GetBool("isOn")) playerTwoCube03Animator.SetBool("isOn", false);
                                    if (playerTwoCube08Animator.GetBool("isOn")) playerTwoCube08Animator.SetBool("isOn", false);
                                    if (playerTwoCube09Animator.GetBool("isOn")) playerTwoCube09Animator.SetBool("isOn", false);
                                
                                    attackNumber = 321;
                                }
                                else if (PlayerMovement.targetPosition.z < -1.5f) // Горизонтальная средняя и вертикальная правая линии (323)
                                {
                                    if (!playerTwoCube03Animator.GetBool("isOn")) playerTwoCube03Animator.SetBool("isOn", true);
                                    if (!playerTwoCube09Animator.GetBool("isOn")) playerTwoCube09Animator.SetBool("isOn", true);
                        
                                    if (playerTwoCube01Animator.GetBool("isOn")) playerTwoCube01Animator.SetBool("isOn", false);
                                    if (playerTwoCube02Animator.GetBool("isOn")) playerTwoCube02Animator.SetBool("isOn", false);
                                    if (playerTwoCube07Animator.GetBool("isOn")) playerTwoCube07Animator.SetBool("isOn", false);
                                    if (playerTwoCube08Animator.GetBool("isOn")) playerTwoCube08Animator.SetBool("isOn", false);
                                
                                    attackNumber = 323;
                                }
                                else // Горизонтальная средняя и вертикальная средняя линии (322)
                                {
                                    if (!playerTwoCube02Animator.GetBool("isOn")) playerTwoCube02Animator.SetBool("isOn", true);
                                    if (!playerTwoCube08Animator.GetBool("isOn")) playerTwoCube08Animator.SetBool("isOn", true);
                        
                                    if (playerTwoCube01Animator.GetBool("isOn")) playerTwoCube01Animator.SetBool("isOn", false);
                                    if (playerTwoCube03Animator.GetBool("isOn")) playerTwoCube03Animator.SetBool("isOn", false);
                                    if (playerTwoCube07Animator.GetBool("isOn")) playerTwoCube07Animator.SetBool("isOn", false);
                                    if (playerTwoCube09Animator.GetBool("isOn")) playerTwoCube09Animator.SetBool("isOn", false);
                                
                                    attackNumber = 322;
                                }
                            }
                            else
                            {
                                if (!playerTwoCube01Animator.GetBool("isOn")) playerTwoCube01Animator.SetBool("isOn", true);
                                if (!playerTwoCube02Animator.GetBool("isOn")) playerTwoCube02Animator.SetBool("isOn", true);
                                if (!playerTwoCube03Animator.GetBool("isOn")) playerTwoCube03Animator.SetBool("isOn", true);
                        
                                if (PlayerMovement.targetPosition.z > 1.5f) // Горизонтальная нижняя и вертикальная левая линии (311)
                                {
                                    if (!playerTwoCube04Animator.GetBool("isOn")) playerTwoCube04Animator.SetBool("isOn", true);
                                    if (!playerTwoCube07Animator.GetBool("isOn")) playerTwoCube07Animator.SetBool("isOn", true);

                                    if (playerTwoCube05Animator.GetBool("isOn")) playerTwoCube05Animator.SetBool("isOn", false);
                                    if (playerTwoCube06Animator.GetBool("isOn")) playerTwoCube06Animator.SetBool("isOn", false);
                                    if (playerTwoCube08Animator.GetBool("isOn")) playerTwoCube08Animator.SetBool("isOn", false);
                                    if (playerTwoCube09Animator.GetBool("isOn")) playerTwoCube09Animator.SetBool("isOn", false);
                                
                                    attackNumber = 311;
                                }
                                else if (PlayerMovement.targetPosition.z < -1.5f) // Горизонтальная нижняя и вертикальная правая линии (313)
                                {
                                    if (!playerTwoCube06Animator.GetBool("isOn")) playerTwoCube06Animator.SetBool("isOn", true);
                                    if (!playerTwoCube09Animator.GetBool("isOn")) playerTwoCube09Animator.SetBool("isOn", true);

                                    if (playerTwoCube04Animator.GetBool("isOn")) playerTwoCube04Animator.SetBool("isOn", false);
                                    if (playerTwoCube05Animator.GetBool("isOn")) playerTwoCube05Animator.SetBool("isOn", false);
                                    if (playerTwoCube07Animator.GetBool("isOn")) playerTwoCube07Animator.SetBool("isOn", false);
                                    if (playerTwoCube08Animator.GetBool("isOn")) playerTwoCube08Animator.SetBool("isOn", false);
                                
                                    attackNumber = 313;
                                }
                                else // Горизонтальная нижняя и вертикальная средняя линии (312)
                                {
                                    if (!playerTwoCube05Animator.GetBool("isOn")) playerTwoCube05Animator.SetBool("isOn", true);
                                    if (!playerTwoCube08Animator.GetBool("isOn")) playerTwoCube08Animator.SetBool("isOn", true);

                                    if (playerTwoCube04Animator.GetBool("isOn")) playerTwoCube04Animator.SetBool("isOn", false);
                                    if (playerTwoCube06Animator.GetBool("isOn")) playerTwoCube06Animator.SetBool("isOn", false);
                                    if (playerTwoCube07Animator.GetBool("isOn")) playerTwoCube07Animator.SetBool("isOn", false);
                                    if (playerTwoCube09Animator.GetBool("isOn")) playerTwoCube09Animator.SetBool("isOn", false);
                                
                                    attackNumber = 312;
                                }
                            }
                        }
                        else
                        {
                            if (PlayerMovement.targetPosition.x > 10.0f) 
                            {
                                if (!playerOneCube07Animator.GetBool("isOn")) playerOneCube07Animator.SetBool("isOn", true);
                                if (!playerOneCube08Animator.GetBool("isOn")) playerOneCube08Animator.SetBool("isOn", true);
                                if (!playerOneCube09Animator.GetBool("isOn")) playerOneCube09Animator.SetBool("isOn", true);
                        
                                if (PlayerMovement.targetPosition.z > 1.5f) // Горизонтальная нижняя и вертикальная правая линии (313)
                                {
                                    if (!playerOneCube01Animator.GetBool("isOn")) playerOneCube01Animator.SetBool("isOn", true);
                                    if (!playerOneCube04Animator.GetBool("isOn")) playerOneCube04Animator.SetBool("isOn", true);
                        
                                    if (playerOneCube02Animator.GetBool("isOn")) playerOneCube02Animator.SetBool("isOn", false);
                                    if (playerOneCube03Animator.GetBool("isOn")) playerOneCube03Animator.SetBool("isOn", false);
                                    if (playerOneCube05Animator.GetBool("isOn")) playerOneCube05Animator.SetBool("isOn", false);
                                    if (playerOneCube06Animator.GetBool("isOn")) playerOneCube06Animator.SetBool("isOn", false);
                                
                                    attackNumber = 313;
                                }
                                else if (PlayerMovement.targetPosition.z < -1.5f) // Горизонтальная нижняя и вертикальная левая линии (311)
                                {
                                    if (!playerOneCube03Animator.GetBool("isOn")) playerOneCube03Animator.SetBool("isOn", true);
                                    if (!playerOneCube06Animator.GetBool("isOn")) playerOneCube06Animator.SetBool("isOn", true);
                        
                                    if (playerOneCube01Animator.GetBool("isOn")) playerOneCube01Animator.SetBool("isOn", false);
                                    if (playerOneCube02Animator.GetBool("isOn")) playerOneCube02Animator.SetBool("isOn", false);
                                    if (playerOneCube04Animator.GetBool("isOn")) playerOneCube04Animator.SetBool("isOn", false);
                                    if (playerOneCube05Animator.GetBool("isOn")) playerOneCube05Animator.SetBool("isOn", false);
                                
                                    attackNumber = 311;
                                }
                                else // Горизонтальная нижняя и вертикальная средняя линии (312)
                                {
                                    if (!playerOneCube02Animator.GetBool("isOn")) playerOneCube02Animator.SetBool("isOn", true);
                                    if (!playerOneCube05Animator.GetBool("isOn")) playerOneCube05Animator.SetBool("isOn", true);
                        
                                    if (playerOneCube01Animator.GetBool("isOn")) playerOneCube01Animator.SetBool("isOn", false);
                                    if (playerOneCube03Animator.GetBool("isOn")) playerOneCube03Animator.SetBool("isOn", false);
                                    if (playerOneCube04Animator.GetBool("isOn")) playerOneCube04Animator.SetBool("isOn", false);
                                    if (playerOneCube06Animator.GetBool("isOn")) playerOneCube06Animator.SetBool("isOn", false);
                                
                                    attackNumber = 312;
                                }
                            }
                            else if (PlayerMovement.targetPosition.x > 5.0f) 
                            {
                                if (!playerOneCube04Animator.GetBool("isOn")) playerOneCube04Animator.SetBool("isOn", true);
                                if (!playerOneCube05Animator.GetBool("isOn")) playerOneCube05Animator.SetBool("isOn", true);
                                if (!playerOneCube06Animator.GetBool("isOn")) playerOneCube06Animator.SetBool("isOn", true);
                        
                                if (PlayerMovement.targetPosition.z > 1.5f) // Горизонтальная средняя и вертикальная правая линии (323)
                                {
                                    if (!playerOneCube01Animator.GetBool("isOn")) playerOneCube01Animator.SetBool("isOn", true);
                                    if (!playerOneCube07Animator.GetBool("isOn")) playerOneCube07Animator.SetBool("isOn", true);
                        
                                    if (playerOneCube02Animator.GetBool("isOn")) playerOneCube02Animator.SetBool("isOn", false);
                                    if (playerOneCube03Animator.GetBool("isOn")) playerOneCube03Animator.SetBool("isOn", false);
                                    if (playerOneCube08Animator.GetBool("isOn")) playerOneCube08Animator.SetBool("isOn", false);
                                    if (playerOneCube09Animator.GetBool("isOn")) playerOneCube09Animator.SetBool("isOn", false);
                                
                                    attackNumber = 323;
                                }
                                else if (PlayerMovement.targetPosition.z < -1.5f) // Горизонтальная средняя и вертикальная левая линии (321)
                                {
                                    if (!playerOneCube03Animator.GetBool("isOn")) playerOneCube03Animator.SetBool("isOn", true);
                                    if (!playerOneCube09Animator.GetBool("isOn")) playerOneCube09Animator.SetBool("isOn", true);
                        
                                    if (playerOneCube01Animator.GetBool("isOn")) playerOneCube01Animator.SetBool("isOn", false);
                                    if (playerOneCube02Animator.GetBool("isOn")) playerOneCube02Animator.SetBool("isOn", false);
                                    if (playerOneCube07Animator.GetBool("isOn")) playerOneCube07Animator.SetBool("isOn", false);
                                    if (playerOneCube08Animator.GetBool("isOn")) playerOneCube08Animator.SetBool("isOn", false);
                                
                                    attackNumber = 321;
                                }
                                else // Горизонтальная средняя и вертикальная средняя линии (322)
                                {
                                    if (!playerOneCube02Animator.GetBool("isOn")) playerOneCube02Animator.SetBool("isOn", true);
                                    if (!playerOneCube08Animator.GetBool("isOn")) playerOneCube08Animator.SetBool("isOn", true);
                        
                                    if (playerOneCube01Animator.GetBool("isOn")) playerOneCube01Animator.SetBool("isOn", false);
                                    if (playerOneCube03Animator.GetBool("isOn")) playerOneCube03Animator.SetBool("isOn", false);
                                    if (playerOneCube07Animator.GetBool("isOn")) playerOneCube07Animator.SetBool("isOn", false);
                                    if (playerOneCube09Animator.GetBool("isOn")) playerOneCube09Animator.SetBool("isOn", false);
                                
                                    attackNumber = 322;
                                }
                            }
                            else
                            {
                                if (!playerOneCube01Animator.GetBool("isOn")) playerOneCube01Animator.SetBool("isOn", true);
                                if (!playerOneCube02Animator.GetBool("isOn")) playerOneCube02Animator.SetBool("isOn", true);
                                if (!playerOneCube03Animator.GetBool("isOn")) playerOneCube03Animator.SetBool("isOn", true);
                        
                                if (PlayerMovement.targetPosition.z > 1.5f) // Горизонтальная верхняя и вертикальная правая линии (333)
                                {
                                    if (!playerOneCube04Animator.GetBool("isOn")) playerOneCube04Animator.SetBool("isOn", true);
                                    if (!playerOneCube07Animator.GetBool("isOn")) playerOneCube07Animator.SetBool("isOn", true);

                                    if (playerOneCube05Animator.GetBool("isOn")) playerOneCube05Animator.SetBool("isOn", false);
                                    if (playerOneCube06Animator.GetBool("isOn")) playerOneCube06Animator.SetBool("isOn", false);
                                    if (playerOneCube08Animator.GetBool("isOn")) playerOneCube08Animator.SetBool("isOn", false);
                                    if (playerOneCube09Animator.GetBool("isOn")) playerOneCube09Animator.SetBool("isOn", false);
                                
                                    attackNumber = 333;
                                }
                                else if (PlayerMovement.targetPosition.z < -1.5f) // Горизонтальная верхняя и вертикальная левая линии (331)
                                {
                                    if (!playerOneCube06Animator.GetBool("isOn")) playerOneCube06Animator.SetBool("isOn", true);
                                    if (!playerOneCube09Animator.GetBool("isOn")) playerOneCube09Animator.SetBool("isOn", true);

                                    if (playerOneCube04Animator.GetBool("isOn")) playerOneCube04Animator.SetBool("isOn", false);
                                    if (playerOneCube05Animator.GetBool("isOn")) playerOneCube05Animator.SetBool("isOn", false);
                                    if (playerOneCube07Animator.GetBool("isOn")) playerOneCube07Animator.SetBool("isOn", false);
                                    if (playerOneCube08Animator.GetBool("isOn")) playerOneCube08Animator.SetBool("isOn", false);
                                
                                    attackNumber = 331;
                                }
                                else // Горизонтальная верхняя и вертикальная средняя линии (332)
                                {
                                    if (!playerOneCube05Animator.GetBool("isOn")) playerOneCube05Animator.SetBool("isOn", true);
                                    if (!playerOneCube08Animator.GetBool("isOn")) playerOneCube08Animator.SetBool("isOn", true);

                                    if (playerOneCube04Animator.GetBool("isOn")) playerOneCube04Animator.SetBool("isOn", false);
                                    if (playerOneCube06Animator.GetBool("isOn")) playerOneCube06Animator.SetBool("isOn", false);
                                    if (playerOneCube07Animator.GetBool("isOn")) playerOneCube07Animator.SetBool("isOn", false);
                                    if (playerOneCube09Animator.GetBool("isOn")) playerOneCube09Animator.SetBool("isOn", false);
                                
                                    attackNumber = 332;
                                }
                            }
                        }
                    }
                }
            }

            if (!GameSettings.isSpellCastingOn)
            {
                if (playerTwoCube02Animator.GetBool("isOn")) playerTwoCube02Animator.SetBool("isOn", false);
                if (playerTwoCube03Animator.GetBool("isOn")) playerTwoCube03Animator.SetBool("isOn", false);
                if (playerTwoCube04Animator.GetBool("isOn")) playerTwoCube04Animator.SetBool("isOn", false);
                if (playerTwoCube05Animator.GetBool("isOn")) playerTwoCube05Animator.SetBool("isOn", false);
                if (playerTwoCube06Animator.GetBool("isOn")) playerTwoCube06Animator.SetBool("isOn", false);
                if (playerTwoCube07Animator.GetBool("isOn")) playerTwoCube07Animator.SetBool("isOn", false);
                if (playerTwoCube08Animator.GetBool("isOn")) playerTwoCube08Animator.SetBool("isOn", false);
                if (playerTwoCube09Animator.GetBool("isOn")) playerTwoCube09Animator.SetBool("isOn", false);
                if (playerOneCube01Animator.GetBool("isOn")) playerOneCube01Animator.SetBool("isOn", false);
                if (playerOneCube02Animator.GetBool("isOn")) playerOneCube02Animator.SetBool("isOn", false);
                if (playerOneCube03Animator.GetBool("isOn")) playerOneCube03Animator.SetBool("isOn", false);
                if (playerOneCube04Animator.GetBool("isOn")) playerOneCube04Animator.SetBool("isOn", false);
                if (playerOneCube05Animator.GetBool("isOn")) playerOneCube05Animator.SetBool("isOn", false);
                if (playerOneCube06Animator.GetBool("isOn")) playerOneCube06Animator.SetBool("isOn", false);
                if (playerOneCube07Animator.GetBool("isOn")) playerOneCube07Animator.SetBool("isOn", false);
                if (playerOneCube08Animator.GetBool("isOn")) playerOneCube08Animator.SetBool("isOn", false);
                if (playerOneCube09Animator.GetBool("isOn")) playerOneCube09Animator.SetBool("isOn", false);
            }
        }
    }
}

