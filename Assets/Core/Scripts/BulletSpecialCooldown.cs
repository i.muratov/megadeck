﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Scripts
{
    public class BulletSpecialCooldown : MonoBehaviour
    {
        private Text bulletSpecialCooldown;

        private void Start()
        {
            bulletSpecialCooldown = GetComponent<Text>();
        }

        private void FixedUpdate()
        {
            if (PlayerShooting.bulletSpecialCooldownCount > 0.0f) bulletSpecialCooldown.text = Math.Round(PlayerShooting.bulletSpecialCooldownCount).ToString();
        }
    }
}