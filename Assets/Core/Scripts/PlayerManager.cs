﻿using UnityEngine;
using Photon.Pun;

namespace Core.Scripts
{
    public class PlayerManager : MonoBehaviourPunCallbacks, IPunObservable
    {
        public static GameObject PlayerInstance;
        
        private void Awake()
        {
            if (!photonView.IsMine) return;

            PlayerInstance = gameObject;
        }

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (photonView.IsMine && stream.IsWriting)
            {
                stream.SendNext(PlayerMovement.targetPosition);
                stream.SendNext(PlayerMovement.isMoving);
                stream.SendNext(PlayerShooting.isFiring);
                if (PlayerHealth.isPlayerOne) stream.SendNext(PlayerHealth.healthPlayerOne);
                if (!PlayerHealth.isPlayerOne) stream.SendNext(PlayerHealth.healthPlayerTwo);
                stream.SendNext(PlayerHealth.isDead);
                //stream.SendNext(PlayerShooting.isFiringSpecial01);
                //stream.SendNext(PlayerShooting.isFiringSpecial02);
                //stream.SendNext(PlayerShooting.isFiringSpecial03);
                stream.SendNext(PlayerShooting.isCastingSingle);
                stream.SendNext(PlayerShooting.attackNumberConfirmed);

                if (PlayerShooting.isFiring) PlayerShooting.isFiring = false;
                //if (PlayerShooting.isFiringSpecial01) PlayerShooting.isFiringSpecial01 = false;
                //if (PlayerShooting.isFiringSpecial02) PlayerShooting.isFiringSpecial02 = false;
                //if (PlayerShooting.isFiringSpecial03) PlayerShooting.isFiringSpecial03 = false;
                if (PlayerShooting.isCastingSingle) PlayerShooting.isCastingSingle = false;
            }
            else if (stream.IsReading)
            {
                PlayerMovement.otherPlayerTargetPosition = (Vector3) stream.ReceiveNext();
                PlayerMovement.isOtherPlayerMoving = (bool) stream.ReceiveNext();
                PlayerShooting.isOtherPlayerFiring = (bool) stream.ReceiveNext();
                if (!PlayerHealth.isPlayerOne) PlayerHealth.healthPlayerOne = (float) stream.ReceiveNext();
                if (PlayerHealth.isPlayerOne) PlayerHealth.healthPlayerTwo = (float) stream.ReceiveNext();
                PlayerHealth.isOtherPlayerDead = (bool) stream.ReceiveNext();
                //PlayerShooting.isOtherPlayerFiringSpecial01 = (bool) stream.ReceiveNext();
                //PlayerShooting.isOtherPlayerFiringSpecial02 = (bool) stream.ReceiveNext();
                //PlayerShooting.isOtherPlayerFiringSpecial03 = (bool) stream.ReceiveNext();
                PlayerShooting.isCastingSingleOtherPlayer = (bool) stream.ReceiveNext();
                PlayerShooting.attackNumberConfirmedOtherPlayer = (int) stream.ReceiveNext();
            }
        }
    }
}