﻿using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Random = UnityEngine.Random;

namespace Core.Scripts
{
    public class PlayerShooting : MonoBehaviourPun
    {
        [SerializeField] private Material materialFriendly;
        [SerializeField] private Material materialHostile;
        [SerializeField] private GameObject bullet;
        [SerializeField] private GameObject bulletSpawnPoint;
        [SerializeField] private float bulletMoveSpeed;
        [SerializeField] private float bulletSpawnDelay;
        [SerializeField] private float bulletCount;
        [SerializeField] private int bulletSpecialDeckMaxAmount;
        //[SerializeField] private GameObject bulletSpecial01;
        //[SerializeField] private GameObject bulletSpecial02;
        //[SerializeField] private GameObject bulletSpecial03;
        //[SerializeField] private GameObject bulletSpecialSpawnPoint;
        //[SerializeField] private float bulletSpecialMoveSpeed;
        //[SerializeField] private float bulletSpecialCount;
        [SerializeField] private float bulletSpecialCooldown;
        [SerializeField] private float bulletSpecialAttackDelay;

        public static readonly List<int> bulletSpecialDeckList = new List<int>();
        public static bool isCasting;
        public static bool isCastingSingle;
        public static bool isCastingOtherPlayer;
        public static bool isCastingSingleOtherPlayer;
        public static bool isFiring;
        public static bool isOtherPlayerFiring;
        //public static bool isFiringSpecial01;
        //public static bool isFiringSpecial02;
        //public static bool isFiringSpecial03;
        //public static bool isOtherPlayerFiringSpecial01;
        //public static bool isOtherPlayerFiringSpecial02;
        //public static bool isOtherPlayerFiringSpecial03;
        public static float bulletSpecialCooldownCount;
        public static float bulletSpecialAttackDelayCount;
        public static float bulletSpecialAttackDelayPublic;
        public static float lagCompensation;
        public static float lagCompensationTime;
        public static int indexSpell01;
        public static int indexSpell02;
        public static int attackNumberConfirmed;
        public static int attackNumberConfirmedOtherPlayer;
        
        //private readonly List<GameObject> bulletSpecial01Pool = new List<GameObject>();
        //private readonly List<GameObject> bulletSpecial02Pool = new List<GameObject>();
        //private readonly List<GameObject> bulletSpecial03Pool = new List<GameObject>();
        private readonly List<GameObject> bulletPool = new List<GameObject>();
        private Vector3 bulletTargetPosition;
        private GameObject bulletFire;
        //private GameObject bulletSpecialFire;
        private float timeCounter;
        
        private void Start()
        {
            for (int i = 0; i < bulletCount; i++) // Пре-создание объектов для автоматической атаки
            {
                GameObject newBullet = Instantiate(bullet, bulletSpawnPoint.transform.position, bulletSpawnPoint.transform.rotation);

                if (photonView.IsMine)
                {
                    newBullet.GetComponent<Renderer>().material = materialFriendly;
                }
                else
                {
                    newBullet.GetComponent<Renderer>().material = materialHostile;
                }
                
                bulletPool.Add(newBullet);
            }
            
            /*
            
            for (int i = 0; i < bulletSpecialCount; i++) // Пре-создание объектов для первой способности
            {
                GameObject newBullet = Instantiate(bulletSpecial01, bulletSpecialSpawnPoint.transform.position, bulletSpecialSpawnPoint.transform.rotation);
                bulletSpecial01Pool.Add(newBullet);
            }
            
            for (int i = 0; i < bulletSpecialCount; i++) // Пре-создание объектов для второй способности
            {
                GameObject newBullet = Instantiate(bulletSpecial02, bulletSpecialSpawnPoint.transform.position, bulletSpecialSpawnPoint.transform.rotation);
                bulletSpecial02Pool.Add(newBullet);
            }
            
            for (int i = 0; i < bulletSpecialCount; i++) // Пре-создание объектов для третьей способности
            {
                GameObject newBullet = Instantiate(bulletSpecial03, bulletSpecialSpawnPoint.transform.position, bulletSpecialSpawnPoint.transform.rotation);
                bulletSpecial03Pool.Add(newBullet);
            }
            
            */
            
            if (bulletSpawnPoint.transform.position.x < 0.0f) // Определение вектора для направления атаки
            {
                bulletTargetPosition = new Vector3(25.0f, 0.0f, 0.0f);
            }
            else
            {
                bulletTargetPosition = new Vector3(-25.0f, 0.0f, 0.0f);
            }
            
            if (photonView.IsMine)
            {
                timeCounter = bulletSpawnDelay;
                
                bulletSpecialCooldownCount = 0.0f;
                bulletSpecialAttackDelayCount = 0.0f;
                bulletSpecialAttackDelayPublic = bulletSpecialAttackDelay;

                for (int i = 0; i < bulletSpecialDeckMaxAmount; i++) // Добавление индексов заклинаний в колоду
                {
                    bulletSpecialDeckList.Add(i);
                }

                DeckShuffle();
                
                lagCompensationTime = PhotonNetwork.GetPing() * 0.01f * 1.5f;
            }
            else
            {
                lagCompensation = 1.0f + PhotonNetwork.GetPing() * 0.001f; // Ускорение всех объектов противника для компенсации задержки
            }
        }

        private void FixedUpdate()
        {
            if (photonView.IsMine && !PlayerHealth.isDead && GameSettings.isAutoAttackOn) // Включение автоматической атаки
            {
                timeCounter = timeCounter - Time.deltaTime;
                
                if (!PlayerMovement.isMoving && timeCounter < 0.0f)
                {
                    GetBullet();
                    
                    isFiring = true;
                    timeCounter = bulletSpawnDelay;
                }
            }
            else if (!photonView.IsMine && !PlayerMovement.isOtherPlayerMoving && !PlayerHealth.isOtherPlayerDead)
            {
                if (isOtherPlayerFiring) // Включение вражеской автоматической атаки
                {
                    GetBullet();
                    
                    isOtherPlayerFiring = false;
                }
                
                /*
                
                else if (isOtherPlayerFiringSpecial01) // Включение первой вражеской способности
                {
                    GetBulletSpecial01();

                    isOtherPlayerFiringSpecial01 = false;
                }
                else if (isOtherPlayerFiringSpecial02) // Включение второй вражеской способности
                {
                    GetBulletSpecial02();

                    isOtherPlayerFiringSpecial02 = false;
                }
                else if (isOtherPlayerFiringSpecial03) // Включение третьей вражеской способности
                {
                    GetBulletSpecial03();

                    isOtherPlayerFiringSpecial03 = false;
                }
                
                */
            }

            for (int i = 0; i < bulletPool.Count; i++) // Перемещение активных объектов автоматической атаки
            {
                if (bulletPool[i].activeInHierarchy)
                {
                    if (photonView.IsMine)
                    {
                        bulletPool[i].transform.position = Vector3.MoveTowards(bulletPool[i].transform.position, bulletPool[i].transform.position + bulletTargetPosition, bulletMoveSpeed * Time.deltaTime);
                    }
                    else
                    {
                        bulletPool[i].transform.position = Vector3.MoveTowards(bulletPool[i].transform.position, bulletPool[i].transform.position + bulletTargetPosition, bulletMoveSpeed * Time.deltaTime * lagCompensation);
                    }
                }
            }
            
            /*
            
            for (int i = 0; i < bulletSpecial01Pool.Count; i++) // Перемещение активных объектов первой способности
            {
                if (bulletSpecial01Pool[i].activeInHierarchy)
                {
                    if (photonView.IsMine)
                    {
                        bulletSpecial01Pool[i].transform.position = Vector3.MoveTowards(bulletSpecial01Pool[i].transform.position, bulletSpecial01Pool[i].transform.position + bulletTargetPosition, bulletSpecialMoveSpeed * Time.deltaTime);
                    }
                    else
                    {
                        bulletSpecial01Pool[i].transform.position = Vector3.MoveTowards(bulletSpecial01Pool[i].transform.position, bulletSpecial01Pool[i].transform.position + bulletTargetPosition, bulletSpecialMoveSpeed * Time.deltaTime * lagCompensation);
                    }
                }
            }
            
            for (int i = 0; i < bulletSpecial02Pool.Count; i++) // Перемещение активных объектов второй способности
            {
                if (bulletSpecial02Pool[i].activeInHierarchy)
                {
                    if (photonView.IsMine)
                    {
                        bulletSpecial02Pool[i].transform.position = Vector3.MoveTowards(bulletSpecial02Pool[i].transform.position, bulletSpecial02Pool[i].transform.position + bulletTargetPosition, bulletSpecialMoveSpeed * Time.deltaTime);
                    }
                    else
                    {
                        bulletSpecial02Pool[i].transform.position = Vector3.MoveTowards(bulletSpecial02Pool[i].transform.position, bulletSpecial02Pool[i].transform.position + bulletTargetPosition, bulletSpecialMoveSpeed * Time.deltaTime * lagCompensation);
                    }
                }
            }
            
            for (int i = 0; i < bulletSpecial03Pool.Count; i++) // Перемещение активных объектов третьей способности
            {
                if (bulletSpecial03Pool[i].activeInHierarchy)
                {
                    if (photonView.IsMine)
                    {
                        bulletSpecial03Pool[i].transform.position = Vector3.MoveTowards(bulletSpecial03Pool[i].transform.position, bulletSpecial03Pool[i].transform.position + bulletTargetPosition, bulletSpecialMoveSpeed * Time.deltaTime);
                    }
                    else
                    {
                        bulletSpecial03Pool[i].transform.position = Vector3.MoveTowards(bulletSpecial03Pool[i].transform.position, bulletSpecial03Pool[i].transform.position + bulletTargetPosition, bulletSpecialMoveSpeed * Time.deltaTime * lagCompensation);
                    }
                }
            }
            
            */

            if (bulletSpecialCooldownCount > 0.0f) bulletSpecialCooldownCount = bulletSpecialCooldownCount - Time.deltaTime;
            if (bulletSpecialAttackDelayCount > 0.0f) bulletSpecialAttackDelayCount = bulletSpecialAttackDelayCount - Time.deltaTime;
        }

        private void GetBullet()
        {
            for (int i = 0; i < bulletPool.Count; i++)
            {
                if (!bulletPool[i].activeInHierarchy)
                {
                    bulletFire = bulletPool[i];
                }
            }

            bulletFire.transform.position = bulletSpawnPoint.transform.position;
            bulletFire.transform.rotation = bulletSpawnPoint.transform.rotation;
            bulletFire.SetActive(true);
        }
        
        /*

        private void GetBulletSpecial01()
        {
            for (int i = 0; i < bulletSpecial01Pool.Count; i++)
            {
                if (!bulletSpecial01Pool[i].activeInHierarchy)
                {
                    bulletSpecialFire = bulletSpecial01Pool[i];
                }
            }

            bulletSpecialFire.transform.position = bulletSpecialSpawnPoint.transform.position;
            bulletSpecialFire.transform.rotation = bulletSpecialSpawnPoint.transform.rotation;
            bulletSpecialFire.SetActive(true);
        }
        
        private void GetBulletSpecial02()
        {
            for (int i = 0; i < bulletSpecial02Pool.Count; i++)
            {
                if (!bulletSpecial02Pool[i].activeInHierarchy)
                {
                    bulletSpecialFire = bulletSpecial02Pool[i];
                }
            }

            bulletSpecialFire.transform.position = bulletSpecialSpawnPoint.transform.position;
            bulletSpecialFire.transform.rotation = bulletSpecialSpawnPoint.transform.rotation;
            bulletSpecialFire.SetActive(true);
        }
        
        private void GetBulletSpecial03()
        {
            for (int i = 0; i < bulletSpecial03Pool.Count; i++)
            {
                if (!bulletSpecial03Pool[i].activeInHierarchy)
                {
                    bulletSpecialFire = bulletSpecial03Pool[i];
                }
            }

            bulletSpecialFire.transform.position = bulletSpecialSpawnPoint.transform.position;
            bulletSpecialFire.transform.rotation = bulletSpecialSpawnPoint.transform.rotation;
            bulletSpecialFire.SetActive(true);
        }
        
        */

        public void UseBulletSpecial()
        {
            if (photonView.IsMine && !PlayerHealth.isDead && bulletSpecialCooldownCount <= 0.0f && GameSettings.isSpellCastingOn)
            {
                /*
                 
                if (indexSpell01 == 0)
                {
                    GetBulletSpecial01();
                    isFiringSpecial01 = true;
                }
                else if (indexSpell01 == 1)
                {
                    GetBulletSpecial02();
                    isFiringSpecial02 = true;
                }
                else if (indexSpell01 == 2)
                {
                    GetBulletSpecial03();
                    isFiringSpecial03 = true;
                }
                
                */

                isCasting = true;
                isCastingSingle = true;
                indexSpell01 = indexSpell02;

                for (int i = 0; i < bulletSpecialDeckMaxAmount; i++)
                {
                    if (bulletSpecialDeckList[i] == indexSpell02)
                    {
                        if (bulletSpecialDeckMaxAmount != i + 1)
                        {
                            indexSpell02 = bulletSpecialDeckList[i + 1];
                            break;
                        }
                        else
                        {
                            for (int j = 0; j < bulletSpecialDeckMaxAmount; j++)
                            {
                                int tempValue = bulletSpecialDeckList[j];
                                int randomIndex = Random.Range(j, bulletSpecialDeckMaxAmount);
                                bulletSpecialDeckList[j] = bulletSpecialDeckList[randomIndex];
                                bulletSpecialDeckList[randomIndex] = tempValue;
                            }

                            for (int j = 0; j < bulletSpecialDeckMaxAmount; j++)
                            {
                                if (bulletSpecialDeckList[j] != indexSpell01)
                                {
                                    indexSpell02 = bulletSpecialDeckList[j];
                                    break;
                                }
                            }
                        }
                    }
                }

                bulletSpecialCooldownCount = bulletSpecialCooldown;
                bulletSpecialAttackDelayCount = bulletSpecialAttackDelay;
                attackNumberConfirmed = EnvironmentController.attackNumber;
            }
        }

        public void DeckShuffle()
        {
            for (int i = 0; i < bulletSpecialDeckMaxAmount; i++)
            {
                int tempValue = bulletSpecialDeckList[i];
                int randomIndex = Random.Range(i, bulletSpecialDeckMaxAmount);
                bulletSpecialDeckList[i] = bulletSpecialDeckList[randomIndex];
                bulletSpecialDeckList[randomIndex] = tempValue;
            }

            indexSpell01 = bulletSpecialDeckList[0];
            indexSpell02 = bulletSpecialDeckList[1];
        }
    }
}