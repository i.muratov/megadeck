﻿using UnityEngine;
using UnityEngine.UI;

namespace Core.Scripts
{
    public class GameSettings : MonoBehaviour
    {
        [SerializeField] private Toggle toggleAutoAttack;
        [SerializeField] private Toggle toggleAimingDot;
        [SerializeField] private Toggle toggleSpellCasting;

        public static bool isAutoAttackOn;
        public static bool isAimingDotOn;
        public static bool isSpellCastingOn;

        private void FixedUpdate()
        {
            isAutoAttackOn = toggleAutoAttack.isOn;
            isAimingDotOn = toggleAimingDot.isOn;
            isSpellCastingOn = toggleSpellCasting.isOn;
        }
    }
}