﻿using Photon.Pun;
using UnityEngine;

namespace Core.Scripts
{
    public class EnemyTargeting : MonoBehaviourPun
    {
        [SerializeField] private GameObject targetingCross;

        private void FixedUpdate()
        {
            /* if (!photonView.IsMine && !PlayerHealth.isDead)
            {
                targetingCross.SetActive(true);
            } */

            if (GameSettings.isAimingDotOn && !PlayerHealth.isDead)
            {
                targetingCross.SetActive(true);
            }
            else
            {
                targetingCross.SetActive(false);
            }
        }
    }
}