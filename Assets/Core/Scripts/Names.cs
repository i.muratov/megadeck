﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Scripts
{
    public class Names : MonoBehaviour
    {
        [SerializeField] private Text playerNameTextLeft;
        [SerializeField] private Text playerNameTextRight;

        private void Start()
        {
            playerNameTextLeft.text = String.Empty;
            playerNameTextRight.text = String.Empty;
        }

        private void FixedUpdate()
        {
            if (GameManager.isPlaying)
            {
                if (!GameManager.isPlayerTwo)
                {
                    playerNameTextLeft.text = GameManager.namePlayerOne;
                    playerNameTextRight.text = GameManager.namePlayerTwo;
                }
                else
                {
                    playerNameTextLeft.text = GameManager.namePlayerTwo;
                    playerNameTextRight.text = GameManager.namePlayerOne;
                }
            }
        }
    }
}