﻿using System;
using Photon.Pun;
using UnityEngine;

namespace Core.Scripts
{
    public class PlayerHealth : MonoBehaviourPun
    {
        [SerializeField] private float maxHealth;
        [SerializeField] private float bulletDamage;
        [SerializeField] private GameObject playerMesh;

        public static float healthPlayerOne;
        public static float healthPlayerTwo;
        public static bool isPlayerOne;
        public static bool isDead;
        public static bool isOtherPlayerDead;
        
        private Collider playerCollider;

        private void Start()
        {
            playerCollider = GetComponent<Collider>();
            
            healthPlayerOne = maxHealth;
            healthPlayerTwo = maxHealth;

            isDead = false;
            isOtherPlayerDead = false;
            
            if (photonView.IsMine && transform.position.x < 0.0f)
            {
                isPlayerOne = true;
            }
        }

        private void FixedUpdate()
        {
            if (photonView.IsMine && isDead)
            {
                playerMesh.SetActive(false);
                playerCollider.enabled = false;
            }
            else if (!photonView.IsMine && isOtherPlayerDead)
            {
                playerMesh.SetActive(false);
                playerCollider.enabled = false;
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Projectile"))
            {
                other.gameObject.SetActive(false);
                
                if (photonView.IsMine)
                {
                    if (isPlayerOne)
                    {
                        healthPlayerOne = healthPlayerOne - bulletDamage;

                        if (healthPlayerOne <= 0.0f)
                        {
                            isDead = true;
                        }
                    }
                    else
                    {
                        healthPlayerTwo = healthPlayerTwo - bulletDamage;
                        
                        if (healthPlayerTwo <= 0.0f)
                        {
                            isDead = true;
                        }
                    }
                }
            }
            
            if (other.gameObject.CompareTag("Dick"))
            {
                if (photonView.IsMine)
                {
                    if (isPlayerOne)
                    {
                        healthPlayerOne = healthPlayerOne - bulletDamage;

                        if (healthPlayerOne <= 0.0f)
                        {
                            isDead = true;
                        }
                    }
                    else
                    {
                        healthPlayerTwo = healthPlayerTwo - bulletDamage;
                        
                        if (healthPlayerTwo <= 0.0f)
                        {
                            isDead = true;
                        }
                    }
                }
            }
        }
    }
}