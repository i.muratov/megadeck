﻿using UnityEngine;

namespace Core.Scripts
{
    public class GameBulletDisabler : MonoBehaviour
    {
        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.CompareTag("Projectile"))
            {
                other.gameObject.SetActive(false);
            }
        }
    }
}

