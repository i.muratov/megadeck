﻿using System;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

namespace Core.Scripts
{
    public class Launcher : MonoBehaviourPunCallbacks
    {
        [SerializeField] private GameObject uiInput;
        [SerializeField] private GameObject uiLoading;

        private bool isConnecting;
        
        private void Awake()
        {
            PhotonNetwork.AutomaticallySyncScene = true;
        }

        private void Start()
        {
            uiInput.SetActive(true);
            uiLoading.SetActive(false);
        }

        public void Connect()
        {
            uiInput.SetActive(false);
            uiLoading.SetActive(true);

            isConnecting = true;
            
            if (PhotonNetwork.IsConnected)
            {
                PhotonNetwork.JoinRandomRoom();
            }
            else
            {
                PhotonNetwork.ConnectUsingSettings();
            }
        }

        public override void OnConnectedToMaster()
        {
            Debug.Log("OnConnectedToMaster()");

            if (isConnecting)
            {
                PhotonNetwork.JoinRandomRoom();
            }
        }

        public override void OnJoinedRoom()
        {
            Debug.Log("OnJoinedRoom()");
            
            PhotonNetwork.LoadLevel("Game");
        }

        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            Debug.Log("OnJoinRandomFailed()");

            PhotonNetwork.CreateRoom(null, new RoomOptions{ MaxPlayers = 2 });
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            Debug.LogWarningFormat("OnDisconnected(). Reason: {0}", cause);
            
            uiInput.SetActive(true);
            uiLoading.SetActive(false);
        }
    }
}