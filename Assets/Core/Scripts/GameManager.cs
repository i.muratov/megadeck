using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Photon.Realtime;

namespace Core.Scripts
{
    public class GameManager : MonoBehaviourPunCallbacks
    {
        [SerializeField] private Camera mainCamera;
        [SerializeField] private bool isTest;
        [SerializeField] private bool isTestAsPlayerTwo;
        [SerializeField] private float spawnCorrectionX;
        
        public static string namePlayerOne = String.Empty;
        public static string namePlayerTwo = String.Empty;
        public static bool isPlaying;
        public static bool isPlayerTwo;

        public GameObject playerStartTile;
        public GameObject enemyStartTile;
        
        private Vector3 playerPosition;
        private Vector3 playerStartTileVector;
        private Vector3 enemyStartTileVector;

        private void Start()
        {
            playerStartTileVector = new Vector3(playerStartTile.transform.position.x - spawnCorrectionX, playerStartTile.transform.position.y, playerStartTile.transform.position.z);
            enemyStartTileVector = new Vector3(enemyStartTile.transform.position.x + spawnCorrectionX, enemyStartTile.transform.position.y, enemyStartTile.transform.position.z);
            
            if (isTest)
            {
                if (!isTestAsPlayerTwo)
                {
                    PhotonNetwork.Instantiate("Player", playerStartTileVector, Quaternion.identity);
                }
                else
                {
                    PhotonNetwork.Instantiate("Player", enemyStartTileVector, transform.rotation * Quaternion.Euler(0.0f, 180.0f, 0.0f));
                    mainCamera.transform.rotation = Quaternion.Euler(45.0f, 270.0f, 0.0f);
                    mainCamera.transform.position = new Vector3 (- mainCamera.transform.position.x,  mainCamera.transform.position.y,  mainCamera.transform.position.z);
                    isPlayerTwo = true;
                }
            }
            
            isPlaying = false;
        }

        private void FixedUpdate()
        {
            if (!isTest && !isPlaying && PhotonNetwork.CurrentRoom.PlayerCount > 1.0f)
            {
                if (namePlayerOne == String.Empty) namePlayerOne = PhotonNetwork.PlayerList[0].NickName;
                if (namePlayerTwo == String.Empty) namePlayerTwo = PhotonNetwork.PlayerList[1].NickName;
                
                if (PhotonNetwork.LocalPlayer.NickName == namePlayerOne)
                {
                    PhotonNetwork.Instantiate("Player", playerStartTileVector, Quaternion.identity);
                }
                else if (PhotonNetwork.LocalPlayer.NickName == namePlayerTwo)
                {
                    PhotonNetwork.Instantiate("Player", enemyStartTileVector, transform.rotation * Quaternion.Euler(0.0f, 180.0f, 0.0f));
                    mainCamera.transform.rotation = Quaternion.Euler(45.0f, 270.0f, 0.0f);
                    mainCamera.transform.position = new Vector3 (- mainCamera.transform.position.x,  mainCamera.transform.position.y,  mainCamera.transform.position.z);
                    isPlayerTwo = true;
                }

                isPlaying = true;
            }
        }

        public override void OnJoinedRoom()
        {
            Debug.Log("OnJoinedRoom()");
        }

        public override void OnLeftRoom()
        {
            SceneManager.LoadScene(0);
        }

        public void LeaveRoom()
        {
            PhotonNetwork.LeaveRoom();
        }

        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            Debug.Log("OnPlayerEnteredRoom()");
        }

        public override void OnPlayerLeftRoom(Player otherPlayer)
        {
            Debug.Log("OnPlayerLeftRoom()");
        }
    }
}
