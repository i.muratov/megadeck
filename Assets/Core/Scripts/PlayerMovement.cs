﻿using UnityEngine;
using Photon.Pun;

namespace Core.Scripts
{
    public class PlayerMovement : MonoBehaviourPun
    {
        
        [SerializeField] private float delayDistance;
        [SerializeField] private float moveSpeed;

        private GameObject playerStartTile;
        
        public static Vector3 otherPlayerTargetPosition;
        public static Vector3 targetPosition;
        public static bool isOtherPlayerMoving;
        public static bool isMoving;

        private float moveDistanceHorizontal;
        private float moveDistanceVertical;
        
        private Vector3 startPosition;

        public void MoveUp()
        {
            if (photonView.IsMine && !isMoving)
            {
                if (!GameManager.isPlayerTwo)
                {
                    if (transform.position.x < startPosition.x + moveDistanceHorizontal - delayDistance)
                    {
                        targetPosition = new Vector3(transform.position.x + moveDistanceHorizontal, transform.position.y, transform.position.z);
                        isMoving = true;
                    }
                }
                else
                {
                    if (transform.position.x > startPosition.x - moveDistanceHorizontal + delayDistance)
                    {
                        targetPosition = new Vector3(transform.position.x - moveDistanceHorizontal, transform.position.y, transform.position.z);
                        isMoving = true;
                    }
                }
            }
        }
        
        public void MoveDown()
        {
            if (photonView.IsMine && !isMoving)
            {
                if (!GameManager.isPlayerTwo)
                {
                    if (transform.position.x > startPosition.x - moveDistanceHorizontal + delayDistance)
                    {
                        targetPosition = new Vector3(transform.position.x - moveDistanceHorizontal, transform.position.y, transform.position.z);
                        isMoving = true;
                    }
                }
                else
                {
                    if (transform.position.x < startPosition.x + moveDistanceHorizontal - delayDistance)
                    {
                        targetPosition = new Vector3(transform.position.x + moveDistanceHorizontal, transform.position.y, transform.position.z);
                        isMoving = true;
                    }
                }
            }
        }
        
        public void MoveRight()
        {
            if (photonView.IsMine && !isMoving)
            {
                if (!GameManager.isPlayerTwo)
                {
                    if (transform.position.z > startPosition.z - moveDistanceVertical + delayDistance)
                    {
                        targetPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z - moveDistanceVertical);
                        isMoving = true;
                    }
                }
                else
                {
                    if (transform.position.z < startPosition.z + moveDistanceVertical - delayDistance)
                    {
                        targetPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z + moveDistanceVertical);
                        isMoving = true;
                    }
                }
            }
        }
        
        public void MoveLeft()
        {
            if (photonView.IsMine && !isMoving)
            {
                if (!GameManager.isPlayerTwo)
                {
                    if (transform.position.z < startPosition.z + moveDistanceVertical - delayDistance)
                    {
                        targetPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z + moveDistanceVertical);
                        isMoving = true;
                    }
                }
                else
                {
                    if (transform.position.z > startPosition.z - moveDistanceVertical + delayDistance)
                    {
                        targetPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z - moveDistanceVertical);
                        isMoving = true;
                    }
                }
            }
        }

        private void Start()
        {
            //ищем дальность шага
            playerStartTile=GameObject.Find("GameManager").GetComponent<GameManager>().playerStartTile;
            moveDistanceHorizontal=Mathf.Abs(playerStartTile.transform.position.x-GameObject.Find("PlayerCube08").transform.position.x);
            moveDistanceVertical=Mathf.Abs(playerStartTile.transform.position.z-GameObject.Find("PlayerCube04").transform.position.z);

            if (photonView.IsMine)
            {
                startPosition = transform.position;
                targetPosition = transform.position;
            }
        }
        
        private void FixedUpdate()
        {
            if (photonView.IsMine)
            {
                if (isMoving)
                {
                    transform.position = Vector3.MoveTowards(transform.position, targetPosition, moveSpeed * Time.deltaTime);

                    if (transform.position == targetPosition)
                    {
                        isMoving = false;
                    }
                }
            }
            else if (!photonView.IsMine)
            {
                if (isOtherPlayerMoving)
                {
                    transform.position = Vector3.MoveTowards(transform.position, otherPlayerTargetPosition, moveSpeed * Time.deltaTime * PlayerShooting.lagCompensation);
                }
                else if (transform.position != otherPlayerTargetPosition)
                {
                    transform.position = otherPlayerTargetPosition;
                }
            }
        }
    }
}