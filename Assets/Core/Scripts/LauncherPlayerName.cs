﻿using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

namespace Core.Scripts
{
    public class LauncherPlayerName : MonoBehaviour
    {
        private string name;
        
        private void Start()
        {
            InputField input = GetComponent<InputField>();

            if (input != null)
            {
                if (PlayerPrefs.HasKey(name))
                {
                    input.text = PlayerPrefs.GetString(name);
                }
            }
        }

        public void SetPlayerName(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                PhotonNetwork.NickName = value;
                
                PlayerPrefs.SetString(name, value);
            }
        }
    }
}