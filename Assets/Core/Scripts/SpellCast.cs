﻿using UnityEngine;

namespace Core.Scripts
{
    public class SpellCast : MonoBehaviour
    {
        [SerializeField] private GameObject playerOneDick01;
        [SerializeField] private GameObject playerOneDick02;
        [SerializeField] private GameObject playerOneDick03;
        [SerializeField] private GameObject playerOneDick04;
        [SerializeField] private GameObject playerOneDick05;
        [SerializeField] private GameObject playerOneDick06;
        [SerializeField] private GameObject playerOneDick07;
        [SerializeField] private GameObject playerOneDick08;
        [SerializeField] private GameObject playerOneDick09;
        [SerializeField] private GameObject playerTwoDick01;
        [SerializeField] private GameObject playerTwoDick02;
        [SerializeField] private GameObject playerTwoDick03;
        [SerializeField] private GameObject playerTwoDick04;
        [SerializeField] private GameObject playerTwoDick05;
        [SerializeField] private GameObject playerTwoDick06;
        [SerializeField] private GameObject playerTwoDick07;
        [SerializeField] private GameObject playerTwoDick08;
        [SerializeField] private GameObject playerTwoDick09;
        
        [SerializeField] private GameObject playerOneDickTargeting01;
        [SerializeField] private GameObject playerOneDickTargeting02;
        [SerializeField] private GameObject playerOneDickTargeting03;
        [SerializeField] private GameObject playerOneDickTargeting04;
        [SerializeField] private GameObject playerOneDickTargeting05;
        [SerializeField] private GameObject playerOneDickTargeting06;
        [SerializeField] private GameObject playerOneDickTargeting07;
        [SerializeField] private GameObject playerOneDickTargeting08;
        [SerializeField] private GameObject playerOneDickTargeting09;
        [SerializeField] private GameObject playerTwoDickTargeting01;
        [SerializeField] private GameObject playerTwoDickTargeting02;
        [SerializeField] private GameObject playerTwoDickTargeting03;
        [SerializeField] private GameObject playerTwoDickTargeting04;
        [SerializeField] private GameObject playerTwoDickTargeting05;
        [SerializeField] private GameObject playerTwoDickTargeting06;
        [SerializeField] private GameObject playerTwoDickTargeting07;
        [SerializeField] private GameObject playerTwoDickTargeting08;
        [SerializeField] private GameObject playerTwoDickTargeting09;
        
        private Animator playerOneDick01Animator;
        private Animator playerOneDick02Animator;
        private Animator playerOneDick03Animator;
        private Animator playerOneDick04Animator;
        private Animator playerOneDick05Animator;
        private Animator playerOneDick06Animator;
        private Animator playerOneDick07Animator;
        private Animator playerOneDick08Animator;
        private Animator playerOneDick09Animator;
        private Animator playerTwoDick01Animator;
        private Animator playerTwoDick02Animator;
        private Animator playerTwoDick03Animator;
        private Animator playerTwoDick04Animator;
        private Animator playerTwoDick05Animator;
        private Animator playerTwoDick06Animator;
        private Animator playerTwoDick07Animator;
        private Animator playerTwoDick08Animator;
        private Animator playerTwoDick09Animator;
        
        private Animator playerOneDickTargeting01Animator;
        private Animator playerOneDickTargeting02Animator;
        private Animator playerOneDickTargeting03Animator;
        private Animator playerOneDickTargeting04Animator;
        private Animator playerOneDickTargeting05Animator;
        private Animator playerOneDickTargeting06Animator;
        private Animator playerOneDickTargeting07Animator;
        private Animator playerOneDickTargeting08Animator;
        private Animator playerOneDickTargeting09Animator;
        private Animator playerTwoDickTargeting01Animator;
        private Animator playerTwoDickTargeting02Animator;
        private Animator playerTwoDickTargeting03Animator;
        private Animator playerTwoDickTargeting04Animator;
        private Animator playerTwoDickTargeting05Animator;
        private Animator playerTwoDickTargeting06Animator;
        private Animator playerTwoDickTargeting07Animator;
        private Animator playerTwoDickTargeting08Animator;
        private Animator playerTwoDickTargeting09Animator;
        
        private int attackNumberConfirmedOtherPlayerPrivate;
        private float bulletSpecialAttackDelayCountPrivate;

        private void Start()
        {
            playerOneDick01Animator = playerOneDick01.GetComponent<Animator>();
            playerOneDick02Animator = playerOneDick02.GetComponent<Animator>();
            playerOneDick03Animator = playerOneDick03.GetComponent<Animator>();
            playerOneDick04Animator = playerOneDick04.GetComponent<Animator>();
            playerOneDick05Animator = playerOneDick05.GetComponent<Animator>();
            playerOneDick06Animator = playerOneDick06.GetComponent<Animator>();
            playerOneDick07Animator = playerOneDick07.GetComponent<Animator>();
            playerOneDick08Animator = playerOneDick08.GetComponent<Animator>();
            playerOneDick09Animator = playerOneDick09.GetComponent<Animator>();
            playerTwoDick01Animator = playerTwoDick01.GetComponent<Animator>();
            playerTwoDick02Animator = playerTwoDick02.GetComponent<Animator>();
            playerTwoDick03Animator = playerTwoDick03.GetComponent<Animator>();
            playerTwoDick04Animator = playerTwoDick04.GetComponent<Animator>();
            playerTwoDick05Animator = playerTwoDick05.GetComponent<Animator>();
            playerTwoDick06Animator = playerTwoDick06.GetComponent<Animator>();
            playerTwoDick07Animator = playerTwoDick07.GetComponent<Animator>();
            playerTwoDick08Animator = playerTwoDick08.GetComponent<Animator>();
            playerTwoDick09Animator = playerTwoDick09.GetComponent<Animator>();
            
            playerOneDickTargeting01Animator = playerOneDickTargeting01.GetComponent<Animator>();
            playerOneDickTargeting02Animator = playerOneDickTargeting02.GetComponent<Animator>();
            playerOneDickTargeting03Animator = playerOneDickTargeting03.GetComponent<Animator>();
            playerOneDickTargeting04Animator = playerOneDickTargeting04.GetComponent<Animator>();
            playerOneDickTargeting05Animator = playerOneDickTargeting05.GetComponent<Animator>();
            playerOneDickTargeting06Animator = playerOneDickTargeting06.GetComponent<Animator>();
            playerOneDickTargeting07Animator = playerOneDickTargeting07.GetComponent<Animator>();
            playerOneDickTargeting08Animator = playerOneDickTargeting08.GetComponent<Animator>();
            playerOneDickTargeting09Animator = playerOneDickTargeting09.GetComponent<Animator>();
            playerTwoDickTargeting01Animator = playerTwoDickTargeting01.GetComponent<Animator>();
            playerTwoDickTargeting02Animator = playerTwoDickTargeting02.GetComponent<Animator>();
            playerTwoDickTargeting03Animator = playerTwoDickTargeting03.GetComponent<Animator>();
            playerTwoDickTargeting04Animator = playerTwoDickTargeting04.GetComponent<Animator>();
            playerTwoDickTargeting05Animator = playerTwoDickTargeting05.GetComponent<Animator>();
            playerTwoDickTargeting06Animator = playerTwoDickTargeting06.GetComponent<Animator>();
            playerTwoDickTargeting07Animator = playerTwoDickTargeting07.GetComponent<Animator>();
            playerTwoDickTargeting08Animator = playerTwoDickTargeting08.GetComponent<Animator>();
            playerTwoDickTargeting09Animator = playerTwoDickTargeting09.GetComponent<Animator>();
        }

        private void FixedUpdate()
        {
            if (PlayerShooting.isCastingSingleOtherPlayer)
            {
                PlayerShooting.isCastingOtherPlayer = true;
                attackNumberConfirmedOtherPlayerPrivate = PlayerShooting.attackNumberConfirmedOtherPlayer;
                bulletSpecialAttackDelayCountPrivate = PlayerShooting.bulletSpecialAttackDelayPublic - PlayerShooting.lagCompensationTime;
                PlayerShooting.isCastingSingleOtherPlayer = false;
            }
            
            if (bulletSpecialAttackDelayCountPrivate > 0.0f) bulletSpecialAttackDelayCountPrivate = bulletSpecialAttackDelayCountPrivate - Time.deltaTime;

            if (PlayerShooting.isCasting)
            {
                if (PlayerShooting.bulletSpecialAttackDelayCount > 0.0f)
                {
                    if (!GameManager.isPlayerTwo)
                    {
                        if (PlayerShooting.attackNumberConfirmed == 110)
                        {
                            if (!playerTwoDickTargeting01Animator.GetBool("isOn")) playerTwoDickTargeting01Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting02Animator.GetBool("isOn")) playerTwoDickTargeting02Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting03Animator.GetBool("isOn")) playerTwoDickTargeting03Animator.SetBool("isOn", true);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 120)
                        {
                            if (!playerTwoDickTargeting04Animator.GetBool("isOn")) playerTwoDickTargeting04Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting05Animator.GetBool("isOn")) playerTwoDickTargeting05Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting06Animator.GetBool("isOn")) playerTwoDickTargeting06Animator.SetBool("isOn", true);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 130)
                        {
                            if (!playerTwoDickTargeting07Animator.GetBool("isOn")) playerTwoDickTargeting07Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting08Animator.GetBool("isOn")) playerTwoDickTargeting08Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting09Animator.GetBool("isOn")) playerTwoDickTargeting09Animator.SetBool("isOn", true);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 210)
                        {
                            if (!playerTwoDickTargeting01Animator.GetBool("isOn")) playerTwoDickTargeting01Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting04Animator.GetBool("isOn")) playerTwoDickTargeting04Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting07Animator.GetBool("isOn")) playerTwoDickTargeting07Animator.SetBool("isOn", true);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 220)
                        {
                            if (!playerTwoDickTargeting02Animator.GetBool("isOn")) playerTwoDickTargeting02Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting05Animator.GetBool("isOn")) playerTwoDickTargeting05Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting08Animator.GetBool("isOn")) playerTwoDickTargeting08Animator.SetBool("isOn", true);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 230)
                        {
                            if (!playerTwoDickTargeting03Animator.GetBool("isOn")) playerTwoDickTargeting03Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting06Animator.GetBool("isOn")) playerTwoDickTargeting06Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting09Animator.GetBool("isOn")) playerTwoDickTargeting09Animator.SetBool("isOn", true);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 311)
                        {
                            if (!playerTwoDickTargeting01Animator.GetBool("isOn")) playerTwoDickTargeting01Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting02Animator.GetBool("isOn")) playerTwoDickTargeting02Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting03Animator.GetBool("isOn")) playerTwoDickTargeting03Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting04Animator.GetBool("isOn")) playerTwoDickTargeting04Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting07Animator.GetBool("isOn")) playerTwoDickTargeting07Animator.SetBool("isOn", true);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 312)
                        {
                            if (!playerTwoDickTargeting01Animator.GetBool("isOn")) playerTwoDickTargeting01Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting02Animator.GetBool("isOn")) playerTwoDickTargeting02Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting03Animator.GetBool("isOn")) playerTwoDickTargeting03Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting05Animator.GetBool("isOn")) playerTwoDickTargeting05Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting08Animator.GetBool("isOn")) playerTwoDickTargeting08Animator.SetBool("isOn", true);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 313)
                        {
                            if (!playerTwoDickTargeting01Animator.GetBool("isOn")) playerTwoDickTargeting01Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting02Animator.GetBool("isOn")) playerTwoDickTargeting02Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting03Animator.GetBool("isOn")) playerTwoDickTargeting03Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting06Animator.GetBool("isOn")) playerTwoDickTargeting06Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting09Animator.GetBool("isOn")) playerTwoDickTargeting09Animator.SetBool("isOn", true);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 321)
                        {
                            if (!playerTwoDickTargeting04Animator.GetBool("isOn")) playerTwoDickTargeting04Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting05Animator.GetBool("isOn")) playerTwoDickTargeting05Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting06Animator.GetBool("isOn")) playerTwoDickTargeting06Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting01Animator.GetBool("isOn")) playerTwoDickTargeting01Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting07Animator.GetBool("isOn")) playerTwoDickTargeting07Animator.SetBool("isOn", true);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 322)
                        {
                            if (!playerTwoDickTargeting04Animator.GetBool("isOn")) playerTwoDickTargeting04Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting05Animator.GetBool("isOn")) playerTwoDickTargeting05Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting06Animator.GetBool("isOn")) playerTwoDickTargeting06Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting02Animator.GetBool("isOn")) playerTwoDickTargeting02Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting08Animator.GetBool("isOn")) playerTwoDickTargeting08Animator.SetBool("isOn", true);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 323)
                        {
                            if (!playerTwoDickTargeting04Animator.GetBool("isOn")) playerTwoDickTargeting04Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting05Animator.GetBool("isOn")) playerTwoDickTargeting05Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting06Animator.GetBool("isOn")) playerTwoDickTargeting06Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting03Animator.GetBool("isOn")) playerTwoDickTargeting03Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting09Animator.GetBool("isOn")) playerTwoDickTargeting09Animator.SetBool("isOn", true);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 331)
                        {
                            if (!playerTwoDickTargeting07Animator.GetBool("isOn")) playerTwoDickTargeting07Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting08Animator.GetBool("isOn")) playerTwoDickTargeting08Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting09Animator.GetBool("isOn")) playerTwoDickTargeting09Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting01Animator.GetBool("isOn")) playerTwoDickTargeting01Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting04Animator.GetBool("isOn")) playerTwoDickTargeting04Animator.SetBool("isOn", true);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 332)
                        {
                            if (!playerTwoDickTargeting07Animator.GetBool("isOn")) playerTwoDickTargeting07Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting08Animator.GetBool("isOn")) playerTwoDickTargeting08Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting09Animator.GetBool("isOn")) playerTwoDickTargeting09Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting02Animator.GetBool("isOn")) playerTwoDickTargeting02Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting05Animator.GetBool("isOn")) playerTwoDickTargeting05Animator.SetBool("isOn", true);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 333)
                        {
                            if (!playerTwoDickTargeting07Animator.GetBool("isOn")) playerTwoDickTargeting07Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting08Animator.GetBool("isOn")) playerTwoDickTargeting08Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting09Animator.GetBool("isOn")) playerTwoDickTargeting09Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting03Animator.GetBool("isOn")) playerTwoDickTargeting03Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting06Animator.GetBool("isOn")) playerTwoDickTargeting06Animator.SetBool("isOn", true);
                        }
                    }
                    else
                    {
                        if (PlayerShooting.attackNumberConfirmed == 110)
                        {
                            if (!playerOneDickTargeting01Animator.GetBool("isOn")) playerOneDickTargeting01Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting02Animator.GetBool("isOn")) playerOneDickTargeting02Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting03Animator.GetBool("isOn")) playerOneDickTargeting03Animator.SetBool("isOn", true);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 120)
                        {
                            if (!playerOneDickTargeting04Animator.GetBool("isOn")) playerOneDickTargeting04Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting05Animator.GetBool("isOn")) playerOneDickTargeting05Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting06Animator.GetBool("isOn")) playerOneDickTargeting06Animator.SetBool("isOn", true);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 130)
                        {
                            if (!playerOneDickTargeting07Animator.GetBool("isOn")) playerOneDickTargeting07Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting08Animator.GetBool("isOn")) playerOneDickTargeting08Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting09Animator.GetBool("isOn")) playerOneDickTargeting09Animator.SetBool("isOn", true);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 210)
                        {
                            if (!playerOneDickTargeting01Animator.GetBool("isOn")) playerOneDickTargeting01Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting04Animator.GetBool("isOn")) playerOneDickTargeting04Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting07Animator.GetBool("isOn")) playerOneDickTargeting07Animator.SetBool("isOn", true);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 220)
                        {
                            if (!playerOneDickTargeting02Animator.GetBool("isOn")) playerOneDickTargeting02Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting05Animator.GetBool("isOn")) playerOneDickTargeting05Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting08Animator.GetBool("isOn")) playerOneDickTargeting08Animator.SetBool("isOn", true);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 230)
                        {
                            if (!playerOneDickTargeting03Animator.GetBool("isOn")) playerOneDickTargeting03Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting06Animator.GetBool("isOn")) playerOneDickTargeting06Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting09Animator.GetBool("isOn")) playerOneDickTargeting09Animator.SetBool("isOn", true);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 311)
                        {
                            if (!playerOneDickTargeting01Animator.GetBool("isOn")) playerOneDickTargeting01Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting02Animator.GetBool("isOn")) playerOneDickTargeting02Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting03Animator.GetBool("isOn")) playerOneDickTargeting03Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting04Animator.GetBool("isOn")) playerOneDickTargeting04Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting07Animator.GetBool("isOn")) playerOneDickTargeting07Animator.SetBool("isOn", true);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 312)
                        {
                            if (!playerOneDickTargeting01Animator.GetBool("isOn")) playerOneDickTargeting01Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting02Animator.GetBool("isOn")) playerOneDickTargeting02Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting03Animator.GetBool("isOn")) playerOneDickTargeting03Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting05Animator.GetBool("isOn")) playerOneDickTargeting05Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting08Animator.GetBool("isOn")) playerOneDickTargeting08Animator.SetBool("isOn", true);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 313)
                        {
                            if (!playerOneDickTargeting01Animator.GetBool("isOn")) playerOneDickTargeting01Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting02Animator.GetBool("isOn")) playerOneDickTargeting02Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting03Animator.GetBool("isOn")) playerOneDickTargeting03Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting06Animator.GetBool("isOn")) playerOneDickTargeting06Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting09Animator.GetBool("isOn")) playerOneDickTargeting09Animator.SetBool("isOn", true);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 321)
                        {
                            if (!playerOneDickTargeting04Animator.GetBool("isOn")) playerOneDickTargeting04Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting05Animator.GetBool("isOn")) playerOneDickTargeting05Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting06Animator.GetBool("isOn")) playerOneDickTargeting06Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting01Animator.GetBool("isOn")) playerOneDickTargeting01Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting07Animator.GetBool("isOn")) playerOneDickTargeting07Animator.SetBool("isOn", true);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 322)
                        {
                            if (!playerOneDickTargeting04Animator.GetBool("isOn")) playerOneDickTargeting04Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting05Animator.GetBool("isOn")) playerOneDickTargeting05Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting06Animator.GetBool("isOn")) playerOneDickTargeting06Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting02Animator.GetBool("isOn")) playerOneDickTargeting02Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting08Animator.GetBool("isOn")) playerOneDickTargeting08Animator.SetBool("isOn", true);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 323)
                        {
                            if (!playerOneDickTargeting04Animator.GetBool("isOn")) playerOneDickTargeting04Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting05Animator.GetBool("isOn")) playerOneDickTargeting05Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting06Animator.GetBool("isOn")) playerOneDickTargeting06Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting03Animator.GetBool("isOn")) playerOneDickTargeting03Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting09Animator.GetBool("isOn")) playerOneDickTargeting09Animator.SetBool("isOn", true);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 331)
                        {
                            if (!playerOneDickTargeting07Animator.GetBool("isOn")) playerOneDickTargeting07Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting08Animator.GetBool("isOn")) playerOneDickTargeting08Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting09Animator.GetBool("isOn")) playerOneDickTargeting09Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting01Animator.GetBool("isOn")) playerOneDickTargeting01Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting04Animator.GetBool("isOn")) playerOneDickTargeting04Animator.SetBool("isOn", true);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 332)
                        {
                            if (!playerOneDickTargeting07Animator.GetBool("isOn")) playerOneDickTargeting07Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting08Animator.GetBool("isOn")) playerOneDickTargeting08Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting09Animator.GetBool("isOn")) playerOneDickTargeting09Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting02Animator.GetBool("isOn")) playerOneDickTargeting02Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting05Animator.GetBool("isOn")) playerOneDickTargeting05Animator.SetBool("isOn", true);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 333)
                        {
                            if (!playerOneDickTargeting07Animator.GetBool("isOn")) playerOneDickTargeting07Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting08Animator.GetBool("isOn")) playerOneDickTargeting08Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting09Animator.GetBool("isOn")) playerOneDickTargeting09Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting03Animator.GetBool("isOn")) playerOneDickTargeting03Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting06Animator.GetBool("isOn")) playerOneDickTargeting06Animator.SetBool("isOn", true);
                        }
                    }
                }
                else
                {
                    if (!GameManager.isPlayerTwo)
                    {
                        if (PlayerShooting.attackNumberConfirmed == 110)
                        {
                            playerTwoDick01Animator.Play("DickAnimation");
                            playerTwoDick02Animator.Play("DickAnimation");
                            playerTwoDick03Animator.Play("DickAnimation");
                            
                            if (playerTwoDickTargeting01Animator.GetBool("isOn")) playerTwoDickTargeting01Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting02Animator.GetBool("isOn")) playerTwoDickTargeting02Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting03Animator.GetBool("isOn")) playerTwoDickTargeting03Animator.SetBool("isOn", false);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 120)
                        {
                            playerTwoDick04Animator.Play("DickAnimation");
                            playerTwoDick05Animator.Play("DickAnimation");
                            playerTwoDick06Animator.Play("DickAnimation");
                            
                            if (playerTwoDickTargeting04Animator.GetBool("isOn")) playerTwoDickTargeting04Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting05Animator.GetBool("isOn")) playerTwoDickTargeting05Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting06Animator.GetBool("isOn")) playerTwoDickTargeting06Animator.SetBool("isOn", false);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 130)
                        {
                            playerTwoDick07Animator.Play("DickAnimation");
                            playerTwoDick08Animator.Play("DickAnimation");
                            playerTwoDick09Animator.Play("DickAnimation");
                            
                            if (playerTwoDickTargeting07Animator.GetBool("isOn")) playerTwoDickTargeting07Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting08Animator.GetBool("isOn")) playerTwoDickTargeting08Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting09Animator.GetBool("isOn")) playerTwoDickTargeting09Animator.SetBool("isOn", false);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 210)
                        {
                            playerTwoDick01Animator.Play("DickAnimation");
                            playerTwoDick04Animator.Play("DickAnimation");
                            playerTwoDick07Animator.Play("DickAnimation");
                            
                            if (playerTwoDickTargeting01Animator.GetBool("isOn")) playerTwoDickTargeting01Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting04Animator.GetBool("isOn")) playerTwoDickTargeting04Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting07Animator.GetBool("isOn")) playerTwoDickTargeting07Animator.SetBool("isOn", false);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 220)
                        {
                            playerTwoDick02Animator.Play("DickAnimation");
                            playerTwoDick05Animator.Play("DickAnimation");
                            playerTwoDick08Animator.Play("DickAnimation");
                            
                            if (playerTwoDickTargeting02Animator.GetBool("isOn")) playerTwoDickTargeting02Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting05Animator.GetBool("isOn")) playerTwoDickTargeting05Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting08Animator.GetBool("isOn")) playerTwoDickTargeting08Animator.SetBool("isOn", false);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 230)
                        {
                            playerTwoDick03Animator.Play("DickAnimation");
                            playerTwoDick06Animator.Play("DickAnimation");
                            playerTwoDick09Animator.Play("DickAnimation");
                            
                            if (playerTwoDickTargeting03Animator.GetBool("isOn")) playerTwoDickTargeting03Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting06Animator.GetBool("isOn")) playerTwoDickTargeting06Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting09Animator.GetBool("isOn")) playerTwoDickTargeting09Animator.SetBool("isOn", false);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 311)
                        {
                            playerTwoDick01Animator.Play("DickAnimation");
                            playerTwoDick02Animator.Play("DickAnimation");
                            playerTwoDick03Animator.Play("DickAnimation");
                            playerTwoDick04Animator.Play("DickAnimation");
                            playerTwoDick07Animator.Play("DickAnimation");
                            
                            if (playerTwoDickTargeting01Animator.GetBool("isOn")) playerTwoDickTargeting01Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting02Animator.GetBool("isOn")) playerTwoDickTargeting02Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting03Animator.GetBool("isOn")) playerTwoDickTargeting03Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting04Animator.GetBool("isOn")) playerTwoDickTargeting04Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting07Animator.GetBool("isOn")) playerTwoDickTargeting07Animator.SetBool("isOn", false);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 312)
                        {
                            playerTwoDick01Animator.Play("DickAnimation");
                            playerTwoDick02Animator.Play("DickAnimation");
                            playerTwoDick03Animator.Play("DickAnimation");
                            playerTwoDick05Animator.Play("DickAnimation");
                            playerTwoDick08Animator.Play("DickAnimation");
                            
                            if (playerTwoDickTargeting01Animator.GetBool("isOn")) playerTwoDickTargeting01Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting02Animator.GetBool("isOn")) playerTwoDickTargeting02Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting03Animator.GetBool("isOn")) playerTwoDickTargeting03Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting05Animator.GetBool("isOn")) playerTwoDickTargeting05Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting08Animator.GetBool("isOn")) playerTwoDickTargeting08Animator.SetBool("isOn", false);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 313)
                        {
                            playerTwoDick01Animator.Play("DickAnimation");
                            playerTwoDick02Animator.Play("DickAnimation");
                            playerTwoDick03Animator.Play("DickAnimation");
                            playerTwoDick06Animator.Play("DickAnimation");
                            playerTwoDick09Animator.Play("DickAnimation");
                            
                            if (playerTwoDickTargeting01Animator.GetBool("isOn")) playerTwoDickTargeting01Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting02Animator.GetBool("isOn")) playerTwoDickTargeting02Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting03Animator.GetBool("isOn")) playerTwoDickTargeting03Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting06Animator.GetBool("isOn")) playerTwoDickTargeting06Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting09Animator.GetBool("isOn")) playerTwoDickTargeting09Animator.SetBool("isOn", false);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 321)
                        {
                            playerTwoDick04Animator.Play("DickAnimation");
                            playerTwoDick05Animator.Play("DickAnimation");
                            playerTwoDick06Animator.Play("DickAnimation");
                            playerTwoDick01Animator.Play("DickAnimation");
                            playerTwoDick07Animator.Play("DickAnimation");
                            
                            if (playerTwoDickTargeting04Animator.GetBool("isOn")) playerTwoDickTargeting04Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting05Animator.GetBool("isOn")) playerTwoDickTargeting05Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting06Animator.GetBool("isOn")) playerTwoDickTargeting06Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting01Animator.GetBool("isOn")) playerTwoDickTargeting01Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting07Animator.GetBool("isOn")) playerTwoDickTargeting07Animator.SetBool("isOn", false);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 322)
                        {
                            playerTwoDick04Animator.Play("DickAnimation");
                            playerTwoDick05Animator.Play("DickAnimation");
                            playerTwoDick06Animator.Play("DickAnimation");
                            playerTwoDick02Animator.Play("DickAnimation");
                            playerTwoDick08Animator.Play("DickAnimation");
                            
                            if (playerTwoDickTargeting04Animator.GetBool("isOn")) playerTwoDickTargeting04Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting05Animator.GetBool("isOn")) playerTwoDickTargeting05Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting06Animator.GetBool("isOn")) playerTwoDickTargeting06Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting02Animator.GetBool("isOn")) playerTwoDickTargeting02Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting08Animator.GetBool("isOn")) playerTwoDickTargeting08Animator.SetBool("isOn", false);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 323)
                        {
                            playerTwoDick04Animator.Play("DickAnimation");
                            playerTwoDick05Animator.Play("DickAnimation");
                            playerTwoDick06Animator.Play("DickAnimation");
                            playerTwoDick03Animator.Play("DickAnimation");
                            playerTwoDick09Animator.Play("DickAnimation");
                            
                            if (playerTwoDickTargeting04Animator.GetBool("isOn")) playerTwoDickTargeting04Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting05Animator.GetBool("isOn")) playerTwoDickTargeting05Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting06Animator.GetBool("isOn")) playerTwoDickTargeting06Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting03Animator.GetBool("isOn")) playerTwoDickTargeting03Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting09Animator.GetBool("isOn")) playerTwoDickTargeting09Animator.SetBool("isOn", false);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 331)
                        {
                            playerTwoDick07Animator.Play("DickAnimation");
                            playerTwoDick08Animator.Play("DickAnimation");
                            playerTwoDick09Animator.Play("DickAnimation");
                            playerTwoDick01Animator.Play("DickAnimation");
                            playerTwoDick04Animator.Play("DickAnimation");
                            
                            if (playerTwoDickTargeting07Animator.GetBool("isOn")) playerTwoDickTargeting07Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting08Animator.GetBool("isOn")) playerTwoDickTargeting08Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting09Animator.GetBool("isOn")) playerTwoDickTargeting09Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting01Animator.GetBool("isOn")) playerTwoDickTargeting01Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting04Animator.GetBool("isOn")) playerTwoDickTargeting04Animator.SetBool("isOn", false);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 332)
                        {
                            playerTwoDick07Animator.Play("DickAnimation");
                            playerTwoDick08Animator.Play("DickAnimation");
                            playerTwoDick09Animator.Play("DickAnimation");
                            playerTwoDick02Animator.Play("DickAnimation");
                            playerTwoDick05Animator.Play("DickAnimation");
                            
                            if (playerTwoDickTargeting07Animator.GetBool("isOn")) playerTwoDickTargeting07Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting08Animator.GetBool("isOn")) playerTwoDickTargeting08Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting09Animator.GetBool("isOn")) playerTwoDickTargeting09Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting02Animator.GetBool("isOn")) playerTwoDickTargeting02Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting05Animator.GetBool("isOn")) playerTwoDickTargeting05Animator.SetBool("isOn", false);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 333)
                        {
                            playerTwoDick07Animator.Play("DickAnimation");
                            playerTwoDick08Animator.Play("DickAnimation");
                            playerTwoDick09Animator.Play("DickAnimation");
                            playerTwoDick03Animator.Play("DickAnimation");
                            playerTwoDick06Animator.Play("DickAnimation");
                            
                            if (playerTwoDickTargeting07Animator.GetBool("isOn")) playerTwoDickTargeting07Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting08Animator.GetBool("isOn")) playerTwoDickTargeting08Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting09Animator.GetBool("isOn")) playerTwoDickTargeting09Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting03Animator.GetBool("isOn")) playerTwoDickTargeting03Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting06Animator.GetBool("isOn")) playerTwoDickTargeting06Animator.SetBool("isOn", false);
                        }
                    }
                    else
                    {
                        if (PlayerShooting.attackNumberConfirmed == 110)
                        {
                            playerOneDick01Animator.Play("DickAnimation");
                            playerOneDick02Animator.Play("DickAnimation");
                            playerOneDick03Animator.Play("DickAnimation");
                            
                            if (playerOneDickTargeting01Animator.GetBool("isOn")) playerOneDickTargeting01Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting02Animator.GetBool("isOn")) playerOneDickTargeting02Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting03Animator.GetBool("isOn")) playerOneDickTargeting03Animator.SetBool("isOn", false);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 120)
                        {
                            playerOneDick04Animator.Play("DickAnimation");
                            playerOneDick05Animator.Play("DickAnimation");
                            playerOneDick06Animator.Play("DickAnimation");
                            
                            if (playerOneDickTargeting04Animator.GetBool("isOn")) playerOneDickTargeting04Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting05Animator.GetBool("isOn")) playerOneDickTargeting05Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting06Animator.GetBool("isOn")) playerOneDickTargeting06Animator.SetBool("isOn", false);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 130)
                        {
                            playerOneDick07Animator.Play("DickAnimation");
                            playerOneDick08Animator.Play("DickAnimation");
                            playerOneDick09Animator.Play("DickAnimation");
                            
                            if (playerOneDickTargeting07Animator.GetBool("isOn")) playerOneDickTargeting07Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting08Animator.GetBool("isOn")) playerOneDickTargeting08Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting09Animator.GetBool("isOn")) playerOneDickTargeting09Animator.SetBool("isOn", false);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 210)
                        {
                            playerOneDick01Animator.Play("DickAnimation");
                            playerOneDick04Animator.Play("DickAnimation");
                            playerOneDick07Animator.Play("DickAnimation");
                            
                            if (playerOneDickTargeting01Animator.GetBool("isOn")) playerOneDickTargeting01Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting04Animator.GetBool("isOn")) playerOneDickTargeting04Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting07Animator.GetBool("isOn")) playerOneDickTargeting07Animator.SetBool("isOn", false);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 220)
                        {
                            playerOneDick02Animator.Play("DickAnimation");
                            playerOneDick05Animator.Play("DickAnimation");
                            playerOneDick08Animator.Play("DickAnimation");
                            
                            if (playerOneDickTargeting02Animator.GetBool("isOn")) playerOneDickTargeting02Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting05Animator.GetBool("isOn")) playerOneDickTargeting05Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting08Animator.GetBool("isOn")) playerOneDickTargeting08Animator.SetBool("isOn", false);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 230)
                        {
                            playerOneDick03Animator.Play("DickAnimation");
                            playerOneDick06Animator.Play("DickAnimation");
                            playerOneDick09Animator.Play("DickAnimation");
                            
                            if (playerOneDickTargeting03Animator.GetBool("isOn")) playerOneDickTargeting03Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting06Animator.GetBool("isOn")) playerOneDickTargeting06Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting09Animator.GetBool("isOn")) playerOneDickTargeting09Animator.SetBool("isOn", false);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 311)
                        {
                            playerOneDick01Animator.Play("DickAnimation");
                            playerOneDick02Animator.Play("DickAnimation");
                            playerOneDick03Animator.Play("DickAnimation");
                            playerOneDick04Animator.Play("DickAnimation");
                            playerOneDick07Animator.Play("DickAnimation");
                            
                            if (playerOneDickTargeting01Animator.GetBool("isOn")) playerOneDickTargeting01Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting02Animator.GetBool("isOn")) playerOneDickTargeting02Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting03Animator.GetBool("isOn")) playerOneDickTargeting03Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting04Animator.GetBool("isOn")) playerOneDickTargeting04Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting07Animator.GetBool("isOn")) playerOneDickTargeting07Animator.SetBool("isOn", false);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 312)
                        {
                            playerOneDick01Animator.Play("DickAnimation");
                            playerOneDick02Animator.Play("DickAnimation");
                            playerOneDick03Animator.Play("DickAnimation");
                            playerOneDick05Animator.Play("DickAnimation");
                            playerOneDick08Animator.Play("DickAnimation");
                            
                            if (playerOneDickTargeting01Animator.GetBool("isOn")) playerOneDickTargeting01Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting02Animator.GetBool("isOn")) playerOneDickTargeting02Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting03Animator.GetBool("isOn")) playerOneDickTargeting03Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting05Animator.GetBool("isOn")) playerOneDickTargeting05Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting08Animator.GetBool("isOn")) playerOneDickTargeting08Animator.SetBool("isOn", false);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 313)
                        {
                            playerOneDick01Animator.Play("DickAnimation");
                            playerOneDick02Animator.Play("DickAnimation");
                            playerOneDick03Animator.Play("DickAnimation");
                            playerOneDick06Animator.Play("DickAnimation");
                            playerOneDick09Animator.Play("DickAnimation");
                            
                            if (playerOneDickTargeting01Animator.GetBool("isOn")) playerOneDickTargeting01Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting02Animator.GetBool("isOn")) playerOneDickTargeting02Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting03Animator.GetBool("isOn")) playerOneDickTargeting03Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting06Animator.GetBool("isOn")) playerOneDickTargeting06Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting09Animator.GetBool("isOn")) playerOneDickTargeting09Animator.SetBool("isOn", false);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 321)
                        {
                            playerOneDick04Animator.Play("DickAnimation");
                            playerOneDick05Animator.Play("DickAnimation");
                            playerOneDick06Animator.Play("DickAnimation");
                            playerOneDick01Animator.Play("DickAnimation");
                            playerOneDick07Animator.Play("DickAnimation");
                            
                            if (playerOneDickTargeting04Animator.GetBool("isOn")) playerOneDickTargeting04Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting05Animator.GetBool("isOn")) playerOneDickTargeting05Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting06Animator.GetBool("isOn")) playerOneDickTargeting06Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting01Animator.GetBool("isOn")) playerOneDickTargeting01Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting07Animator.GetBool("isOn")) playerOneDickTargeting07Animator.SetBool("isOn", false);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 322)
                        {
                            playerOneDick04Animator.Play("DickAnimation");
                            playerOneDick05Animator.Play("DickAnimation");
                            playerOneDick06Animator.Play("DickAnimation");
                            playerOneDick02Animator.Play("DickAnimation");
                            playerOneDick08Animator.Play("DickAnimation");
                            
                            if (playerOneDickTargeting04Animator.GetBool("isOn")) playerOneDickTargeting04Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting05Animator.GetBool("isOn")) playerOneDickTargeting05Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting06Animator.GetBool("isOn")) playerOneDickTargeting06Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting02Animator.GetBool("isOn")) playerOneDickTargeting02Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting08Animator.GetBool("isOn")) playerOneDickTargeting08Animator.SetBool("isOn", false);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 323)
                        {
                            playerOneDick04Animator.Play("DickAnimation");
                            playerOneDick05Animator.Play("DickAnimation");
                            playerOneDick06Animator.Play("DickAnimation");
                            playerOneDick03Animator.Play("DickAnimation");
                            playerOneDick09Animator.Play("DickAnimation");
                            
                            if (playerOneDickTargeting04Animator.GetBool("isOn")) playerOneDickTargeting04Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting05Animator.GetBool("isOn")) playerOneDickTargeting05Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting06Animator.GetBool("isOn")) playerOneDickTargeting06Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting03Animator.GetBool("isOn")) playerOneDickTargeting03Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting09Animator.GetBool("isOn")) playerOneDickTargeting09Animator.SetBool("isOn", false);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 331)
                        {
                            playerOneDick07Animator.Play("DickAnimation");
                            playerOneDick08Animator.Play("DickAnimation");
                            playerOneDick09Animator.Play("DickAnimation");
                            playerOneDick01Animator.Play("DickAnimation");
                            playerOneDick04Animator.Play("DickAnimation");
                            
                            if (playerOneDickTargeting07Animator.GetBool("isOn")) playerOneDickTargeting07Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting08Animator.GetBool("isOn")) playerOneDickTargeting08Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting09Animator.GetBool("isOn")) playerOneDickTargeting09Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting01Animator.GetBool("isOn")) playerOneDickTargeting01Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting04Animator.GetBool("isOn")) playerOneDickTargeting04Animator.SetBool("isOn", false);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 332)
                        {
                            playerOneDick07Animator.Play("DickAnimation");
                            playerOneDick08Animator.Play("DickAnimation");
                            playerOneDick09Animator.Play("DickAnimation");
                            playerOneDick02Animator.Play("DickAnimation");
                            playerOneDick05Animator.Play("DickAnimation");
                            
                            if (playerOneDickTargeting07Animator.GetBool("isOn")) playerOneDickTargeting07Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting08Animator.GetBool("isOn")) playerOneDickTargeting08Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting09Animator.GetBool("isOn")) playerOneDickTargeting09Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting02Animator.GetBool("isOn")) playerOneDickTargeting02Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting05Animator.GetBool("isOn")) playerOneDickTargeting05Animator.SetBool("isOn", false);
                        }
                        else if (PlayerShooting.attackNumberConfirmed == 333)
                        {
                            playerOneDick07Animator.Play("DickAnimation");
                            playerOneDick08Animator.Play("DickAnimation");
                            playerOneDick09Animator.Play("DickAnimation");
                            playerOneDick03Animator.Play("DickAnimation");
                            playerOneDick06Animator.Play("DickAnimation");
                            
                            if (playerOneDickTargeting07Animator.GetBool("isOn")) playerOneDickTargeting07Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting08Animator.GetBool("isOn")) playerOneDickTargeting08Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting09Animator.GetBool("isOn")) playerOneDickTargeting09Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting03Animator.GetBool("isOn")) playerOneDickTargeting03Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting06Animator.GetBool("isOn")) playerOneDickTargeting06Animator.SetBool("isOn", false);
                        }
                    }

                    PlayerShooting.isCasting = false;
                }
            }
            
            if (PlayerShooting.isCastingOtherPlayer)
            {
                if (bulletSpecialAttackDelayCountPrivate > 0.0f)
                {
                    if (!GameManager.isPlayerTwo)
                    {
                        if (attackNumberConfirmedOtherPlayerPrivate == 110)
                        {
                            if (!playerOneDickTargeting01Animator.GetBool("isOn")) playerOneDickTargeting01Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting02Animator.GetBool("isOn")) playerOneDickTargeting02Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting03Animator.GetBool("isOn")) playerOneDickTargeting03Animator.SetBool("isOn", true);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 120)
                        {
                            if (!playerOneDickTargeting04Animator.GetBool("isOn")) playerOneDickTargeting04Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting05Animator.GetBool("isOn")) playerOneDickTargeting05Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting06Animator.GetBool("isOn")) playerOneDickTargeting06Animator.SetBool("isOn", true);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 130)
                        {
                            if (!playerOneDickTargeting07Animator.GetBool("isOn")) playerOneDickTargeting07Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting08Animator.GetBool("isOn")) playerOneDickTargeting08Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting09Animator.GetBool("isOn")) playerOneDickTargeting09Animator.SetBool("isOn", true);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 210)
                        {
                            if (!playerOneDickTargeting01Animator.GetBool("isOn")) playerOneDickTargeting01Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting04Animator.GetBool("isOn")) playerOneDickTargeting04Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting07Animator.GetBool("isOn")) playerOneDickTargeting07Animator.SetBool("isOn", true);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 220)
                        {
                            if (!playerOneDickTargeting02Animator.GetBool("isOn")) playerOneDickTargeting02Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting05Animator.GetBool("isOn")) playerOneDickTargeting05Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting08Animator.GetBool("isOn")) playerOneDickTargeting08Animator.SetBool("isOn", true);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 230)
                        {
                            if (!playerOneDickTargeting03Animator.GetBool("isOn")) playerOneDickTargeting03Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting06Animator.GetBool("isOn")) playerOneDickTargeting06Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting09Animator.GetBool("isOn")) playerOneDickTargeting09Animator.SetBool("isOn", true);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 311)
                        {
                            if (!playerOneDickTargeting01Animator.GetBool("isOn")) playerOneDickTargeting01Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting02Animator.GetBool("isOn")) playerOneDickTargeting02Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting03Animator.GetBool("isOn")) playerOneDickTargeting03Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting04Animator.GetBool("isOn")) playerOneDickTargeting04Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting07Animator.GetBool("isOn")) playerOneDickTargeting07Animator.SetBool("isOn", true);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 312)
                        {
                            if (!playerOneDickTargeting01Animator.GetBool("isOn")) playerOneDickTargeting01Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting02Animator.GetBool("isOn")) playerOneDickTargeting02Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting03Animator.GetBool("isOn")) playerOneDickTargeting03Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting05Animator.GetBool("isOn")) playerOneDickTargeting05Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting08Animator.GetBool("isOn")) playerOneDickTargeting08Animator.SetBool("isOn", true);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 313)
                        {
                            if (!playerOneDickTargeting01Animator.GetBool("isOn")) playerOneDickTargeting01Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting02Animator.GetBool("isOn")) playerOneDickTargeting02Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting03Animator.GetBool("isOn")) playerOneDickTargeting03Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting06Animator.GetBool("isOn")) playerOneDickTargeting06Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting09Animator.GetBool("isOn")) playerOneDickTargeting09Animator.SetBool("isOn", true);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 321)
                        {
                            if (!playerOneDickTargeting04Animator.GetBool("isOn")) playerOneDickTargeting04Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting05Animator.GetBool("isOn")) playerOneDickTargeting05Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting06Animator.GetBool("isOn")) playerOneDickTargeting06Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting01Animator.GetBool("isOn")) playerOneDickTargeting01Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting07Animator.GetBool("isOn")) playerOneDickTargeting07Animator.SetBool("isOn", true);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 322)
                        {
                            if (!playerOneDickTargeting04Animator.GetBool("isOn")) playerOneDickTargeting04Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting05Animator.GetBool("isOn")) playerOneDickTargeting05Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting06Animator.GetBool("isOn")) playerOneDickTargeting06Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting02Animator.GetBool("isOn")) playerOneDickTargeting02Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting08Animator.GetBool("isOn")) playerOneDickTargeting08Animator.SetBool("isOn", true);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 323)
                        {
                            if (!playerOneDickTargeting04Animator.GetBool("isOn")) playerOneDickTargeting04Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting05Animator.GetBool("isOn")) playerOneDickTargeting05Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting06Animator.GetBool("isOn")) playerOneDickTargeting06Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting03Animator.GetBool("isOn")) playerOneDickTargeting03Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting09Animator.GetBool("isOn")) playerOneDickTargeting09Animator.SetBool("isOn", true);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 331)
                        {
                            if (!playerOneDickTargeting07Animator.GetBool("isOn")) playerOneDickTargeting07Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting08Animator.GetBool("isOn")) playerOneDickTargeting08Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting09Animator.GetBool("isOn")) playerOneDickTargeting09Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting01Animator.GetBool("isOn")) playerOneDickTargeting01Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting04Animator.GetBool("isOn")) playerOneDickTargeting04Animator.SetBool("isOn", true);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 332)
                        {
                            if (!playerOneDickTargeting07Animator.GetBool("isOn")) playerOneDickTargeting07Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting08Animator.GetBool("isOn")) playerOneDickTargeting08Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting09Animator.GetBool("isOn")) playerOneDickTargeting09Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting02Animator.GetBool("isOn")) playerOneDickTargeting02Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting05Animator.GetBool("isOn")) playerOneDickTargeting05Animator.SetBool("isOn", true);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 333)
                        {
                            if (!playerOneDickTargeting07Animator.GetBool("isOn")) playerOneDickTargeting07Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting08Animator.GetBool("isOn")) playerOneDickTargeting08Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting09Animator.GetBool("isOn")) playerOneDickTargeting09Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting03Animator.GetBool("isOn")) playerOneDickTargeting03Animator.SetBool("isOn", true);
                            if (!playerOneDickTargeting06Animator.GetBool("isOn")) playerOneDickTargeting06Animator.SetBool("isOn", true);
                        }
                    }
                    else
                    {
                        if (attackNumberConfirmedOtherPlayerPrivate == 110)
                        {
                            if (!playerTwoDickTargeting01Animator.GetBool("isOn")) playerTwoDickTargeting01Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting02Animator.GetBool("isOn")) playerTwoDickTargeting02Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting03Animator.GetBool("isOn")) playerTwoDickTargeting03Animator.SetBool("isOn", true);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 120)
                        {
                            if (!playerTwoDickTargeting04Animator.GetBool("isOn")) playerTwoDickTargeting04Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting05Animator.GetBool("isOn")) playerTwoDickTargeting05Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting06Animator.GetBool("isOn")) playerTwoDickTargeting06Animator.SetBool("isOn", true);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 130)
                        {
                            if (!playerTwoDickTargeting07Animator.GetBool("isOn")) playerTwoDickTargeting07Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting08Animator.GetBool("isOn")) playerTwoDickTargeting08Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting09Animator.GetBool("isOn")) playerTwoDickTargeting09Animator.SetBool("isOn", true);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 210)
                        {
                            if (!playerTwoDickTargeting01Animator.GetBool("isOn")) playerTwoDickTargeting01Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting04Animator.GetBool("isOn")) playerTwoDickTargeting04Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting07Animator.GetBool("isOn")) playerTwoDickTargeting07Animator.SetBool("isOn", true);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 220)
                        {
                            if (!playerTwoDickTargeting02Animator.GetBool("isOn")) playerTwoDickTargeting02Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting05Animator.GetBool("isOn")) playerTwoDickTargeting05Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting08Animator.GetBool("isOn")) playerTwoDickTargeting08Animator.SetBool("isOn", true);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 230)
                        {
                            if (!playerTwoDickTargeting03Animator.GetBool("isOn")) playerTwoDickTargeting03Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting06Animator.GetBool("isOn")) playerTwoDickTargeting06Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting09Animator.GetBool("isOn")) playerTwoDickTargeting09Animator.SetBool("isOn", true);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 311)
                        {
                            if (!playerTwoDickTargeting01Animator.GetBool("isOn")) playerTwoDickTargeting01Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting02Animator.GetBool("isOn")) playerTwoDickTargeting02Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting03Animator.GetBool("isOn")) playerTwoDickTargeting03Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting04Animator.GetBool("isOn")) playerTwoDickTargeting04Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting07Animator.GetBool("isOn")) playerTwoDickTargeting07Animator.SetBool("isOn", true);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 312)
                        {
                            if (!playerTwoDickTargeting01Animator.GetBool("isOn")) playerTwoDickTargeting01Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting02Animator.GetBool("isOn")) playerTwoDickTargeting02Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting03Animator.GetBool("isOn")) playerTwoDickTargeting03Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting05Animator.GetBool("isOn")) playerTwoDickTargeting05Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting08Animator.GetBool("isOn")) playerTwoDickTargeting08Animator.SetBool("isOn", true);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 313)
                        {
                            if (!playerTwoDickTargeting01Animator.GetBool("isOn")) playerTwoDickTargeting01Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting02Animator.GetBool("isOn")) playerTwoDickTargeting02Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting03Animator.GetBool("isOn")) playerTwoDickTargeting03Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting06Animator.GetBool("isOn")) playerTwoDickTargeting06Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting09Animator.GetBool("isOn")) playerTwoDickTargeting09Animator.SetBool("isOn", true);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 321)
                        {
                            if (!playerTwoDickTargeting04Animator.GetBool("isOn")) playerTwoDickTargeting04Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting05Animator.GetBool("isOn")) playerTwoDickTargeting05Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting06Animator.GetBool("isOn")) playerTwoDickTargeting06Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting01Animator.GetBool("isOn")) playerTwoDickTargeting01Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting07Animator.GetBool("isOn")) playerTwoDickTargeting07Animator.SetBool("isOn", true);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 322)
                        {
                            if (!playerTwoDickTargeting04Animator.GetBool("isOn")) playerTwoDickTargeting04Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting05Animator.GetBool("isOn")) playerTwoDickTargeting05Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting06Animator.GetBool("isOn")) playerTwoDickTargeting06Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting02Animator.GetBool("isOn")) playerTwoDickTargeting02Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting08Animator.GetBool("isOn")) playerTwoDickTargeting08Animator.SetBool("isOn", true);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 323)
                        {
                            if (!playerTwoDickTargeting04Animator.GetBool("isOn")) playerTwoDickTargeting04Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting05Animator.GetBool("isOn")) playerTwoDickTargeting05Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting06Animator.GetBool("isOn")) playerTwoDickTargeting06Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting03Animator.GetBool("isOn")) playerTwoDickTargeting03Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting09Animator.GetBool("isOn")) playerTwoDickTargeting09Animator.SetBool("isOn", true);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 331)
                        {
                            if (!playerTwoDickTargeting07Animator.GetBool("isOn")) playerTwoDickTargeting07Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting08Animator.GetBool("isOn")) playerTwoDickTargeting08Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting09Animator.GetBool("isOn")) playerTwoDickTargeting09Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting01Animator.GetBool("isOn")) playerTwoDickTargeting01Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting04Animator.GetBool("isOn")) playerTwoDickTargeting04Animator.SetBool("isOn", true);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 332)
                        {
                            if (!playerTwoDickTargeting07Animator.GetBool("isOn")) playerTwoDickTargeting07Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting08Animator.GetBool("isOn")) playerTwoDickTargeting08Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting09Animator.GetBool("isOn")) playerTwoDickTargeting09Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting02Animator.GetBool("isOn")) playerTwoDickTargeting02Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting05Animator.GetBool("isOn")) playerTwoDickTargeting05Animator.SetBool("isOn", true);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 333)
                        {
                            if (!playerTwoDickTargeting07Animator.GetBool("isOn")) playerTwoDickTargeting07Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting08Animator.GetBool("isOn")) playerTwoDickTargeting08Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting09Animator.GetBool("isOn")) playerTwoDickTargeting09Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting03Animator.GetBool("isOn")) playerTwoDickTargeting03Animator.SetBool("isOn", true);
                            if (!playerTwoDickTargeting06Animator.GetBool("isOn")) playerTwoDickTargeting06Animator.SetBool("isOn", true);
                        }
                    }
                }
                else
                {
                    if (!GameManager.isPlayerTwo)
                    {
                        if (attackNumberConfirmedOtherPlayerPrivate == 110)
                        {
                            playerOneDick01Animator.Play("DickAnimation");
                            playerOneDick02Animator.Play("DickAnimation");
                            playerOneDick03Animator.Play("DickAnimation");
                            
                            if (playerOneDickTargeting01Animator.GetBool("isOn")) playerOneDickTargeting01Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting02Animator.GetBool("isOn")) playerOneDickTargeting02Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting03Animator.GetBool("isOn")) playerOneDickTargeting03Animator.SetBool("isOn", false);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 120)
                        {
                            playerOneDick04Animator.Play("DickAnimation");
                            playerOneDick05Animator.Play("DickAnimation");
                            playerOneDick06Animator.Play("DickAnimation");
                            
                            if (playerOneDickTargeting04Animator.GetBool("isOn")) playerOneDickTargeting04Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting05Animator.GetBool("isOn")) playerOneDickTargeting05Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting06Animator.GetBool("isOn")) playerOneDickTargeting06Animator.SetBool("isOn", false);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 130)
                        {
                            playerOneDick07Animator.Play("DickAnimation");
                            playerOneDick08Animator.Play("DickAnimation");
                            playerOneDick09Animator.Play("DickAnimation");
                            
                            if (playerOneDickTargeting07Animator.GetBool("isOn")) playerOneDickTargeting07Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting08Animator.GetBool("isOn")) playerOneDickTargeting08Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting09Animator.GetBool("isOn")) playerOneDickTargeting09Animator.SetBool("isOn", false);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 210)
                        {
                            playerOneDick01Animator.Play("DickAnimation");
                            playerOneDick04Animator.Play("DickAnimation");
                            playerOneDick07Animator.Play("DickAnimation");
                            
                            if (playerOneDickTargeting01Animator.GetBool("isOn")) playerOneDickTargeting01Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting04Animator.GetBool("isOn")) playerOneDickTargeting04Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting07Animator.GetBool("isOn")) playerOneDickTargeting07Animator.SetBool("isOn", false);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 220)
                        {
                            playerOneDick02Animator.Play("DickAnimation");
                            playerOneDick05Animator.Play("DickAnimation");
                            playerOneDick08Animator.Play("DickAnimation");
                            
                            if (playerOneDickTargeting02Animator.GetBool("isOn")) playerOneDickTargeting02Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting05Animator.GetBool("isOn")) playerOneDickTargeting05Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting08Animator.GetBool("isOn")) playerOneDickTargeting08Animator.SetBool("isOn", false);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 230)
                        {
                            playerOneDick03Animator.Play("DickAnimation");
                            playerOneDick06Animator.Play("DickAnimation");
                            playerOneDick09Animator.Play("DickAnimation");
                            
                            if (playerOneDickTargeting03Animator.GetBool("isOn")) playerOneDickTargeting03Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting06Animator.GetBool("isOn")) playerOneDickTargeting06Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting09Animator.GetBool("isOn")) playerOneDickTargeting09Animator.SetBool("isOn", false);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 311)
                        {
                            playerOneDick01Animator.Play("DickAnimation");
                            playerOneDick02Animator.Play("DickAnimation");
                            playerOneDick03Animator.Play("DickAnimation");
                            playerOneDick04Animator.Play("DickAnimation");
                            playerOneDick07Animator.Play("DickAnimation");
                            
                            if (playerOneDickTargeting01Animator.GetBool("isOn")) playerOneDickTargeting01Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting02Animator.GetBool("isOn")) playerOneDickTargeting02Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting03Animator.GetBool("isOn")) playerOneDickTargeting03Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting04Animator.GetBool("isOn")) playerOneDickTargeting04Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting07Animator.GetBool("isOn")) playerOneDickTargeting07Animator.SetBool("isOn", false);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 312)
                        {
                            playerOneDick01Animator.Play("DickAnimation");
                            playerOneDick02Animator.Play("DickAnimation");
                            playerOneDick03Animator.Play("DickAnimation");
                            playerOneDick05Animator.Play("DickAnimation");
                            playerOneDick08Animator.Play("DickAnimation");
                            
                            if (playerOneDickTargeting01Animator.GetBool("isOn")) playerOneDickTargeting01Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting02Animator.GetBool("isOn")) playerOneDickTargeting02Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting03Animator.GetBool("isOn")) playerOneDickTargeting03Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting05Animator.GetBool("isOn")) playerOneDickTargeting05Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting08Animator.GetBool("isOn")) playerOneDickTargeting08Animator.SetBool("isOn", false);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 313)
                        {
                            playerOneDick01Animator.Play("DickAnimation");
                            playerOneDick02Animator.Play("DickAnimation");
                            playerOneDick03Animator.Play("DickAnimation");
                            playerOneDick06Animator.Play("DickAnimation");
                            playerOneDick09Animator.Play("DickAnimation");
                            
                            if (playerOneDickTargeting01Animator.GetBool("isOn")) playerOneDickTargeting01Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting02Animator.GetBool("isOn")) playerOneDickTargeting02Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting03Animator.GetBool("isOn")) playerOneDickTargeting03Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting06Animator.GetBool("isOn")) playerOneDickTargeting06Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting09Animator.GetBool("isOn")) playerOneDickTargeting09Animator.SetBool("isOn", false);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 321)
                        {
                            playerOneDick04Animator.Play("DickAnimation");
                            playerOneDick05Animator.Play("DickAnimation");
                            playerOneDick06Animator.Play("DickAnimation");
                            playerOneDick01Animator.Play("DickAnimation");
                            playerOneDick07Animator.Play("DickAnimation");
                            
                            if (playerOneDickTargeting04Animator.GetBool("isOn")) playerOneDickTargeting04Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting05Animator.GetBool("isOn")) playerOneDickTargeting05Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting06Animator.GetBool("isOn")) playerOneDickTargeting06Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting01Animator.GetBool("isOn")) playerOneDickTargeting01Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting07Animator.GetBool("isOn")) playerOneDickTargeting07Animator.SetBool("isOn", false);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 322)
                        {
                            playerOneDick04Animator.Play("DickAnimation");
                            playerOneDick05Animator.Play("DickAnimation");
                            playerOneDick06Animator.Play("DickAnimation");
                            playerOneDick02Animator.Play("DickAnimation");
                            playerOneDick08Animator.Play("DickAnimation");
                            
                            if (playerOneDickTargeting04Animator.GetBool("isOn")) playerOneDickTargeting04Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting05Animator.GetBool("isOn")) playerOneDickTargeting05Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting06Animator.GetBool("isOn")) playerOneDickTargeting06Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting02Animator.GetBool("isOn")) playerOneDickTargeting02Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting08Animator.GetBool("isOn")) playerOneDickTargeting08Animator.SetBool("isOn", false);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 323)
                        {
                            playerOneDick04Animator.Play("DickAnimation");
                            playerOneDick05Animator.Play("DickAnimation");
                            playerOneDick06Animator.Play("DickAnimation");
                            playerOneDick03Animator.Play("DickAnimation");
                            playerOneDick09Animator.Play("DickAnimation");
                            
                            if (playerOneDickTargeting04Animator.GetBool("isOn")) playerOneDickTargeting04Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting05Animator.GetBool("isOn")) playerOneDickTargeting05Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting06Animator.GetBool("isOn")) playerOneDickTargeting06Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting03Animator.GetBool("isOn")) playerOneDickTargeting03Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting09Animator.GetBool("isOn")) playerOneDickTargeting09Animator.SetBool("isOn", false);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 331)
                        {
                            playerOneDick07Animator.Play("DickAnimation");
                            playerOneDick08Animator.Play("DickAnimation");
                            playerOneDick09Animator.Play("DickAnimation");
                            playerOneDick01Animator.Play("DickAnimation");
                            playerOneDick04Animator.Play("DickAnimation");
                            
                            if (playerOneDickTargeting07Animator.GetBool("isOn")) playerOneDickTargeting07Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting08Animator.GetBool("isOn")) playerOneDickTargeting08Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting09Animator.GetBool("isOn")) playerOneDickTargeting09Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting01Animator.GetBool("isOn")) playerOneDickTargeting01Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting04Animator.GetBool("isOn")) playerOneDickTargeting04Animator.SetBool("isOn", false);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 332)
                        {
                            playerOneDick07Animator.Play("DickAnimation");
                            playerOneDick08Animator.Play("DickAnimation");
                            playerOneDick09Animator.Play("DickAnimation");
                            playerOneDick02Animator.Play("DickAnimation");
                            playerOneDick05Animator.Play("DickAnimation");
                            
                            if (playerOneDickTargeting07Animator.GetBool("isOn")) playerOneDickTargeting07Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting08Animator.GetBool("isOn")) playerOneDickTargeting08Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting09Animator.GetBool("isOn")) playerOneDickTargeting09Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting02Animator.GetBool("isOn")) playerOneDickTargeting02Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting05Animator.GetBool("isOn")) playerOneDickTargeting05Animator.SetBool("isOn", false);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 333)
                        {
                            playerOneDick07Animator.Play("DickAnimation");
                            playerOneDick08Animator.Play("DickAnimation");
                            playerOneDick09Animator.Play("DickAnimation");
                            playerOneDick03Animator.Play("DickAnimation");
                            playerOneDick06Animator.Play("DickAnimation");
                            
                            if (playerOneDickTargeting07Animator.GetBool("isOn")) playerOneDickTargeting07Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting08Animator.GetBool("isOn")) playerOneDickTargeting08Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting09Animator.GetBool("isOn")) playerOneDickTargeting09Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting03Animator.GetBool("isOn")) playerOneDickTargeting03Animator.SetBool("isOn", false);
                            if (playerOneDickTargeting06Animator.GetBool("isOn")) playerOneDickTargeting06Animator.SetBool("isOn", false);
                        }
                    }
                    else
                    {
                        if (attackNumberConfirmedOtherPlayerPrivate == 110)
                        {
                            playerTwoDick01Animator.Play("DickAnimation");
                            playerTwoDick02Animator.Play("DickAnimation");
                            playerTwoDick03Animator.Play("DickAnimation");
                            
                            if (playerTwoDickTargeting01Animator.GetBool("isOn")) playerTwoDickTargeting01Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting02Animator.GetBool("isOn")) playerTwoDickTargeting02Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting03Animator.GetBool("isOn")) playerTwoDickTargeting03Animator.SetBool("isOn", false);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 120)
                        {
                            playerTwoDick04Animator.Play("DickAnimation");
                            playerTwoDick05Animator.Play("DickAnimation");
                            playerTwoDick06Animator.Play("DickAnimation");
                            
                            if (playerTwoDickTargeting04Animator.GetBool("isOn")) playerTwoDickTargeting04Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting05Animator.GetBool("isOn")) playerTwoDickTargeting05Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting06Animator.GetBool("isOn")) playerTwoDickTargeting06Animator.SetBool("isOn", false);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 130)
                        {
                            playerTwoDick07Animator.Play("DickAnimation");
                            playerTwoDick08Animator.Play("DickAnimation");
                            playerTwoDick09Animator.Play("DickAnimation");
                            
                            if (playerTwoDickTargeting07Animator.GetBool("isOn")) playerTwoDickTargeting07Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting08Animator.GetBool("isOn")) playerTwoDickTargeting08Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting09Animator.GetBool("isOn")) playerTwoDickTargeting09Animator.SetBool("isOn", false);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 210)
                        {
                            playerTwoDick01Animator.Play("DickAnimation");
                            playerTwoDick04Animator.Play("DickAnimation");
                            playerTwoDick07Animator.Play("DickAnimation");
                            
                            if (playerTwoDickTargeting01Animator.GetBool("isOn")) playerTwoDickTargeting01Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting04Animator.GetBool("isOn")) playerTwoDickTargeting04Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting07Animator.GetBool("isOn")) playerTwoDickTargeting07Animator.SetBool("isOn", false);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 220)
                        {
                            playerTwoDick02Animator.Play("DickAnimation");
                            playerTwoDick05Animator.Play("DickAnimation");
                            playerTwoDick08Animator.Play("DickAnimation");
                            
                            if (playerTwoDickTargeting02Animator.GetBool("isOn")) playerTwoDickTargeting02Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting05Animator.GetBool("isOn")) playerTwoDickTargeting05Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting08Animator.GetBool("isOn")) playerTwoDickTargeting08Animator.SetBool("isOn", false);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 230)
                        {
                            playerTwoDick03Animator.Play("DickAnimation");
                            playerTwoDick06Animator.Play("DickAnimation");
                            playerTwoDick09Animator.Play("DickAnimation");
                            
                            if (playerTwoDickTargeting03Animator.GetBool("isOn")) playerTwoDickTargeting03Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting06Animator.GetBool("isOn")) playerTwoDickTargeting06Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting09Animator.GetBool("isOn")) playerTwoDickTargeting09Animator.SetBool("isOn", false);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 311)
                        {
                            playerTwoDick01Animator.Play("DickAnimation");
                            playerTwoDick02Animator.Play("DickAnimation");
                            playerTwoDick03Animator.Play("DickAnimation");
                            playerTwoDick04Animator.Play("DickAnimation");
                            playerTwoDick07Animator.Play("DickAnimation");
                            
                            if (playerTwoDickTargeting01Animator.GetBool("isOn")) playerTwoDickTargeting01Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting02Animator.GetBool("isOn")) playerTwoDickTargeting02Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting03Animator.GetBool("isOn")) playerTwoDickTargeting03Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting04Animator.GetBool("isOn")) playerTwoDickTargeting04Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting07Animator.GetBool("isOn")) playerTwoDickTargeting07Animator.SetBool("isOn", false);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 312)
                        {
                            playerTwoDick01Animator.Play("DickAnimation");
                            playerTwoDick02Animator.Play("DickAnimation");
                            playerTwoDick03Animator.Play("DickAnimation");
                            playerTwoDick05Animator.Play("DickAnimation");
                            playerTwoDick08Animator.Play("DickAnimation");
                            
                            if (playerTwoDickTargeting01Animator.GetBool("isOn")) playerTwoDickTargeting01Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting02Animator.GetBool("isOn")) playerTwoDickTargeting02Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting03Animator.GetBool("isOn")) playerTwoDickTargeting03Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting05Animator.GetBool("isOn")) playerTwoDickTargeting05Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting08Animator.GetBool("isOn")) playerTwoDickTargeting08Animator.SetBool("isOn", false);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 313)
                        {
                            playerTwoDick01Animator.Play("DickAnimation");
                            playerTwoDick02Animator.Play("DickAnimation");
                            playerTwoDick03Animator.Play("DickAnimation");
                            playerTwoDick06Animator.Play("DickAnimation");
                            playerTwoDick09Animator.Play("DickAnimation");
                            
                            if (playerTwoDickTargeting01Animator.GetBool("isOn")) playerTwoDickTargeting01Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting02Animator.GetBool("isOn")) playerTwoDickTargeting02Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting03Animator.GetBool("isOn")) playerTwoDickTargeting03Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting06Animator.GetBool("isOn")) playerTwoDickTargeting06Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting09Animator.GetBool("isOn")) playerTwoDickTargeting09Animator.SetBool("isOn", false);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 321)
                        {
                            playerTwoDick04Animator.Play("DickAnimation");
                            playerTwoDick05Animator.Play("DickAnimation");
                            playerTwoDick06Animator.Play("DickAnimation");
                            playerTwoDick01Animator.Play("DickAnimation");
                            playerTwoDick07Animator.Play("DickAnimation");
                            
                            if (playerTwoDickTargeting04Animator.GetBool("isOn")) playerTwoDickTargeting04Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting05Animator.GetBool("isOn")) playerTwoDickTargeting05Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting06Animator.GetBool("isOn")) playerTwoDickTargeting06Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting01Animator.GetBool("isOn")) playerTwoDickTargeting01Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting07Animator.GetBool("isOn")) playerTwoDickTargeting07Animator.SetBool("isOn", false);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 322)
                        {
                            playerTwoDick04Animator.Play("DickAnimation");
                            playerTwoDick05Animator.Play("DickAnimation");
                            playerTwoDick06Animator.Play("DickAnimation");
                            playerTwoDick02Animator.Play("DickAnimation");
                            playerTwoDick08Animator.Play("DickAnimation");
                            
                            if (playerTwoDickTargeting04Animator.GetBool("isOn")) playerTwoDickTargeting04Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting05Animator.GetBool("isOn")) playerTwoDickTargeting05Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting06Animator.GetBool("isOn")) playerTwoDickTargeting06Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting02Animator.GetBool("isOn")) playerTwoDickTargeting02Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting08Animator.GetBool("isOn")) playerTwoDickTargeting08Animator.SetBool("isOn", false);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 323)
                        {
                            playerTwoDick04Animator.Play("DickAnimation");
                            playerTwoDick05Animator.Play("DickAnimation");
                            playerTwoDick06Animator.Play("DickAnimation");
                            playerTwoDick03Animator.Play("DickAnimation");
                            playerTwoDick09Animator.Play("DickAnimation");
                            
                            if (playerTwoDickTargeting04Animator.GetBool("isOn")) playerTwoDickTargeting04Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting05Animator.GetBool("isOn")) playerTwoDickTargeting05Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting06Animator.GetBool("isOn")) playerTwoDickTargeting06Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting03Animator.GetBool("isOn")) playerTwoDickTargeting03Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting09Animator.GetBool("isOn")) playerTwoDickTargeting09Animator.SetBool("isOn", false);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 331)
                        {
                            playerTwoDick07Animator.Play("DickAnimation");
                            playerTwoDick08Animator.Play("DickAnimation");
                            playerTwoDick09Animator.Play("DickAnimation");
                            playerTwoDick01Animator.Play("DickAnimation");
                            playerTwoDick04Animator.Play("DickAnimation");
                            
                            if (playerTwoDickTargeting07Animator.GetBool("isOn")) playerTwoDickTargeting07Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting08Animator.GetBool("isOn")) playerTwoDickTargeting08Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting09Animator.GetBool("isOn")) playerTwoDickTargeting09Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting01Animator.GetBool("isOn")) playerTwoDickTargeting01Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting04Animator.GetBool("isOn")) playerTwoDickTargeting04Animator.SetBool("isOn", false);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 332)
                        {
                            playerTwoDick07Animator.Play("DickAnimation");
                            playerTwoDick08Animator.Play("DickAnimation");
                            playerTwoDick09Animator.Play("DickAnimation");
                            playerTwoDick02Animator.Play("DickAnimation");
                            playerTwoDick05Animator.Play("DickAnimation");
                            
                            if (playerTwoDickTargeting07Animator.GetBool("isOn")) playerTwoDickTargeting07Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting08Animator.GetBool("isOn")) playerTwoDickTargeting08Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting09Animator.GetBool("isOn")) playerTwoDickTargeting09Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting02Animator.GetBool("isOn")) playerTwoDickTargeting02Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting05Animator.GetBool("isOn")) playerTwoDickTargeting05Animator.SetBool("isOn", false);
                        }
                        else if (attackNumberConfirmedOtherPlayerPrivate == 333)
                        {
                            playerTwoDick07Animator.Play("DickAnimation");
                            playerTwoDick08Animator.Play("DickAnimation");
                            playerTwoDick09Animator.Play("DickAnimation");
                            playerTwoDick03Animator.Play("DickAnimation");
                            playerTwoDick06Animator.Play("DickAnimation");
                            
                            if (playerTwoDickTargeting07Animator.GetBool("isOn")) playerTwoDickTargeting07Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting08Animator.GetBool("isOn")) playerTwoDickTargeting08Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting09Animator.GetBool("isOn")) playerTwoDickTargeting09Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting03Animator.GetBool("isOn")) playerTwoDickTargeting03Animator.SetBool("isOn", false);
                            if (playerTwoDickTargeting06Animator.GetBool("isOn")) playerTwoDickTargeting06Animator.SetBool("isOn", false);
                        }
                    }
                    
                    PlayerShooting.isCastingOtherPlayer = false;
                }
            }
        }
    }
}