﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Scripts
{
    public class SpellPanel : MonoBehaviour
    {
        [SerializeField] private Image imageActive;
        [SerializeField] private Image imagePrediction;

        private void FixedUpdate()
        {
            if (PlayerHealth.isDead) gameObject.SetActive(false);
            
            if (PlayerShooting.indexSpell01 == 0) imageActive.color = Color.green;
            if (PlayerShooting.indexSpell01 == 1) imageActive.color = Color.yellow;
            if (PlayerShooting.indexSpell01 == 2) imageActive.color = Color.red;
            
            if (PlayerShooting.indexSpell02 == 0) imagePrediction.color = Color.green;
            if (PlayerShooting.indexSpell02 == 1) imagePrediction.color = Color.yellow;
            if (PlayerShooting.indexSpell02 == 2) imagePrediction.color = Color.red;
        }
    }
}