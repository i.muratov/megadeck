﻿using UnityEngine;
using UnityEngine.UI;

namespace Core.Scripts
{
    public class HealthBar : MonoBehaviour
    {
        [SerializeField] private Slider leftHealthBar;
        [SerializeField] private Slider rightHealthBar;

        private void FixedUpdate()
        {
            if (!GameManager.isPlayerTwo)
            {
                leftHealthBar.value = PlayerHealth.healthPlayerOne;
                rightHealthBar.value = PlayerHealth.healthPlayerTwo;
            }
            else
            {
                leftHealthBar.value = PlayerHealth.healthPlayerTwo;
                rightHealthBar.value = PlayerHealth.healthPlayerOne;
            }
        }
    }
}